const express = require("express");
const http = require("http");
const cors = require('cors')
require('dotenv').config();

const { dbConnection } = require('./src/db/config');
const { createSocket } = require("./src/TransitCenterInformationServices/socket");

const PORT = process.env.PORT || 4000;

// IMPORT ROUTES
const routeRoutes = require('./src/PretripTravelInformation/routes/routes.routes')
const vehicleRoutes = require('./src/VehiclesManagement/routes/vehicles.routes');
const reportsRoutes = require('./src/TransitCenterInformationServices/routes/reports.routes');
const stationsRoutes = require('./src/PretripTravelInformation/routes/stations.routes');
const eventsRoutes = require('./src/TransitCenterInformationServices/routes/events.routes');

// DATABASE CONNECTION
dbConnection()

// CREATE EXPRESS SERVER
const app = express();
app.use(cors())

app.use(express.json());

// API ROUTES
app.use('/api/routes', routeRoutes)
app.use('/api/vehicles', vehicleRoutes)
app.use('/api/reports', reportsRoutes)
app.use('/api/stations', stationsRoutes)
app.use('/api/events', eventsRoutes)

// CREATE SERVER
const server = http.createServer(app);

// CREATE SOCKET
createSocket(server)

server.listen(PORT, () => console.log(`Listening on PORT ${PORT}`));