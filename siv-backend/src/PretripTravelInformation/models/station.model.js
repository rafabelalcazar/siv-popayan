const mongoose = require('mongoose')

const Schema = mongoose.Schema


var stationSchema = new Schema({
    name: { type: String, required: [true, 'El nombre es requerido'] },
    location: [],
})


module.exports = mongoose.model('Station', stationSchema)