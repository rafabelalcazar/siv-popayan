const mongoose = require('mongoose')

const Schema = mongoose.Schema

var routeSchema = new Schema({
    name: { type: String, required: [true, 'El nombre es requerido'] },
    org_uid: { type: String, required: [true, 'El uid de empresa es requerido'] },
    fare: { type: Number, required: [true, 'La tarifa es requerida'] },
    status: { type: Boolean, default: true },
    color: { type: String, default: '#444444' },
    path: []

})

module.exports = mongoose.model('Route', routeSchema)