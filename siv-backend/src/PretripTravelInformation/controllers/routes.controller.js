const { response } = require('express')
const Route = require('../models/route.model')

const controller = {

    /**
     * Returns all routes.
     *
     * @param {Object} request http request.
     * @param {Object} response http response.
     * @return {Object} http response with routes.
     */
    showAll: (req, res) => {
        Route.find({}, (err, routes) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    msg: 'Can´t get routes',
                    err
                })
            }
            res.status(200).json({
                ok: true,
                routes
            })
        })
    },

    /**
     * Returns all routes by enterprise, filtering by org_uid.
     *
     * @param {Object} request http request.body must contains (org_uid) .
     * @param {Object} response http response.
     * @return {Object} http response with enterprise routes.
     */
    showEnterpriseRoutes: (req, res) => {
        const { org_uid } = req.body
        Route.find({org_uid: org_uid}, (err, routes) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    msg: 'Can´t get routes',
                    err
                })
            }
            res.status(200).json({
                ok: true,
                routes
            })
        })
    },

    /**
     * Returns one route with a specific id.
     *
     * @param {Object} request http request.params must contains (id).
     * @param {Object} response http response.
     * @return {Object} http response with a route data.
     */
    getOne: (req, res) => {
        const id = req.params.id
        Route.findById(id, (err, route) => {

            if (err) {
                return res.status(500).json({
                    ok: false,
                    msg: 'Can´t find the route',
                    err
                })
            }

            if (!route) {
                return res.status(400).json({
                    ok: false,
                    msg: `This route with this id ${id} doesn´t exists`
                })
            }

            res.status(200).json({
                ok: true,
                route
            })

        })
    },

    /**
     * Create a route.
     *
     * @param {Object} request http request.body must contains Route Model Data.
     * @param {Object} response http response.
     * @return {Object} http response confirmation.
     */
    create: (req, res) => {

        const body = req.body

        const route = new Route({
            name: body.name,
            fare: body.fare,
            path: body.path,
            status: body.status,
            color: body.color,
            org_uid: body.org_uid
        })


        route.save((err, routeCreated) => {
            if (err) {
                console.log(err.message)
                return res.status(500).json({
                    ok: false,
                    msg: 'Can´t create route',
                    err
                })
            }

            res.status(201).json({
                ok: true,
                msg: 'route created',
                routeCreated,
                routeToken: req.route
            })
        })
    },

    /**
     * update a route.
     *
     * @param {Object} request http request.body must contains Route Model Data.
     * @param {Object} response http response.
     * @return {Object} http response confirmation.
     */
    update: (req, res) => {
        const id = req.params.id
        body = req.body


        Route.findById(id, (err, route) => {

            if (err) {
                return res.status(500).json({
                    ok: false,
                    msg: 'Can´t find the route',
                    err
                })
            }

            if (!route) {
                return res.status(400).json({
                    ok: false,
                    msg: `This route with this id ${id} doesn´t exists`
                })
            }

            route.name = body.name ? body.name : route.name
            route.fare = body.fare ? body.fare : route.fare
            route.path = body.path ? body.path : route.path
            route.color = body.color ? body.color : route.color
            route.status = body.status ? body.status : route.status

            route.save((err, x) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        msg: `Error updating route`,
                        err
                    })
                }

                x.password = ':)'

                res.status(200).json({
                    ok: true,
                    msg: 'route updated',
                    x
                })
            })
        })
    },

    /**
     * delete a route.
     *
     * @param {Object} request http request.params must contains id.
     * @param {Object} response http response.
     * @return {Object} http response confirmation.
     */
    delete: (req, res) => {

        const id = req.params.id

        Route.findByIdAndDelete(id, (err, routeDeleted) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    msg: 'Error deleting route',
                    err
                })
            }
            if (!routeDeleted) {
                return res.status(400).json({
                    ok: false,
                    msg: `This route with this id ${id} doesn´t exists `,
                    err
                })
            }

            routeDeleted.password = ':)'

            res.status(200).json({
                ok: true,
                msg: 'route deleted',
                routeDeleted
            })
        })
    }
}

module.exports = controller
