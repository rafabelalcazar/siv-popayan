var Station = require('../models/station.model')

const controller = {

    /**
     * Returns all stations.
     *
     * @param {Object} request http request.
     * @param {Object} response http response.
     * @return {Object} http response with all stations.
     */
    showAll: async (req, res) => {
        Station.find({}, (err, stations) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    msg: 'Can´t get stations',
                    err
                })
            }
            res.status(200).json({
                ok: true,
                stations
            })
        })
    },

    /**
     * Returns one station with a specific id.
     *
     * @param {Object} request http request.params must contains (id).
     * @param {Object} response http response.
     * @return {Object} http response with a station data.
     */
    getOne: async (req, res) => {
        const id = req.params.id

        Station.findById(id, async (err, station) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    msg: 'Can´t find the station',
                    err
                })
            }

            if (!station) {
                return res.status(400).json({
                    ok: false,
                    msg: `This station with this id ${id} doesn´t exists`
                })
            }

            res.status(200).json({
                ok: true,
                station
            })
        })
    }
}

module.exports = controller