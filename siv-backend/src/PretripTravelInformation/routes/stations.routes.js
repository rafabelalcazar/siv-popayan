const { Router, response } = require("express");

// IMPORTS
const stationController = require('../controllers/station.controller')

const router = Router()


// Methods for stations
router.get('/', stationController.showAll)
router.get('/:id', stationController.getOne)

module.exports = router