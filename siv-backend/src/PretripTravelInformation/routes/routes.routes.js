const { Router, response } = require("express");

// IMPORTS
const routeController = require("../controllers/routes.controller");

const router = Router()

// Methods for routes
router.post('/new', routeController.create)
router.get('/', [], routeController.showAll)
router.get('/:id', [], routeController.getOne)
router.put('/:id', [], routeController.update)
router.post('/enterprise', [], routeController.showEnterpriseRoutes)

module.exports = router