const mongoose = require('mongoose');

/**
 * Set connection with the database.
 *
 */
const dbConnection =  async () => {
    try {
        mongoose.set('useCreateIndex', true)
        mongoose.connect(process.env.DB, {
            useNewUrlParser: true, 
            useUnifiedTopology: true,
            useCreateIndex: true,
        },
        (err, res) => {
            if(err) throw err
            console.log('\x1b[32mDatabase connection successful')
        }
         )

        
    } catch (error) {
        console.log(error)
        throw new Error('Error DB connections')
        
    }
}

module.exports = { dbConnection }