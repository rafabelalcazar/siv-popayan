const mongoose = require('mongoose')

const Schema = mongoose.Schema


var transitSchema = new Schema({
    counter: { type: String},
    payload_raw: { type: String },
    metadata: { type: String },
    confirmed: { type: String },
    hardware_serial: { type: String},
    port: { type: String},
    app_id: { type: String},
    payload_fields: { type: String},
    dev_id: { type: String},
    creation_date: { type: String},
    downlink_url: { type: String},
    is_retry: { type: String},
    velocity: { type: String},
    passengers: { type: String},
})


module.exports = mongoose.model('Transit', transitSchema)