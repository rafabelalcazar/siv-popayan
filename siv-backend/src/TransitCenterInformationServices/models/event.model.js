const mongoose = require('mongoose')

const Schema = mongoose.Schema


var eventSchema = new Schema({
    type: { type: String, required: [true, 'El tipo es requerido'] },
    location: [],
    creation_date: { type: String}
}
)

module.exports = mongoose.model('Event', eventSchema)