const { Router, response } = require("express");

// IMPORTS
const eventController = require('../controllers/events.controller')

const router = Router()

// Methods for events
router.post('/period', eventController.showPeriod)
router.post('/', eventController.create)

module.exports = router