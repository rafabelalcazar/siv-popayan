const { Router, response } = require("express");

// IMPORTS
const reportsController = require("../controllers/reports.controller")

const router = Router()

// Methods for reports
router.post('/',[], reportsController.showGeneralReport )
router.post('/:serial',[], reportsController.getTransitVehicle )

module.exports = router