const Event = require('../models/event.model')

const controller = {

    /**
     * Returns all events from an initialDate.
     *
     * @param {Object} request http request.body must contains (initialDate, type).
     * @param {Object} response http response.
     * @return {Object} http response with events.
     */
    showPeriod: async (req, res) => {
        
        const {initialDate, type} = req.body

        const conditionInitialDate = {
            "creation_date": { $gte: initialDate },
            ...(type && {"type": type})
        }

        const events = await Event.find(conditionInitialDate).sort('-creation_date')

        if(!events){
            return res.status(400).json({
                ok:false,
                msg: 'No events found'
            })
        }

        res.status(200).json({
            ok:true,
            events
        })
    },

    /**
     * Create an event.
     *
     * @param {Object} request http request.body must contains Event Model data .
     * @param {Object} response http response.
     * @return {Object} http response confirmation.
     */
    create: (req, res) => {

        const body = req.body

        const event = new Event({
            type: body.type,
            location: body.location,
            creation_date: body.creation_date
        })

        event.save((err, eventCreated) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    msg: 'Can´t create event',
                    err
                })
            }

            res.status(201).json({
                ok: true,
                msg: 'event created',
                eventCreated,
                eventToken: req.event
            })
        })
    },

}

module.exports = controller