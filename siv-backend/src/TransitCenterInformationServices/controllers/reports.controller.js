const { response } = require('express')

const Transit = require('../models/transit.model')

const controller = {

    /**
     * Returns a list with all traffic records.
     *
     * @param {Object} request http request.body must contains (initialDate, endDate).
     * @param {Object} response http response.
     * @return {Object} http response with traffic records.
     */
    showGeneralReport: async (req, res) => {

        const { initialDate, endDate } = req.body

        const conditionInitialDate = {
            "creation_date": { $gte: initialDate }
        }

        const conditionEndDate = {
            "creation_date": { $lte: endDate }
        }

        const transits = await Transit.find(conditionInitialDate).find(conditionEndDate).sort("-creation_date")

        if (!transits) {
            return res.status(400).json({
                ok: false,
                msg: `No data`
            })
        }

        res.status(200).json({
            ok: true,
            transits
        })
    },

    /**
     * Returns a list of all traffic records of a vehicle associated with a hardware serial.
     *
     * @param {Object} request http request.body must contains (initialDate, endDate).
     * @param {Object} response http response.
     * @return {Object} http response with traffic records.
     */
    getTransitVehicle: async (req, res) => {
        const hardware_serial = req.params.serial

        const { initialDate, endDate } = req.body

        const conditionInitialDate = {
            "creation_date": { $gte: initialDate },
            "hardware_serial": hardware_serial
        }

        const conditionEndDate = {
            "creation_date": { $lte: endDate }
        }

        const transits = await Transit.find(conditionInitialDate).find(conditionEndDate).sort("-creation_date")

        if (!transits) {
            return res.status(400).json({
                ok: false,
                msg: `No data`
            })
        }

        res.status(200).json({
            ok: true,
            transits
        })

    }

}

module.exports = controller
