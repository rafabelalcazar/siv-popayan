const socketIo = require("socket.io");

const Vehicle = require('../VehiclesManagement/models/vehicle.model')
const Transit = require('./models/transit.model')

/**
 * Create a socket from an existing server.
 *
 * @param {Object} server server.
 */
const createSocket = server => {

    const io = socketIo(server, {
        cors: {
            // origin: "http://localhost:3000",
            origin: "*",
            // methods: ["GET", "POST"]
        }
    });


    io.on("connection", (socket) => {
        console.log("New client connected", socket.id);

        socket.on("disconnect", () => {
            console.log("Client disconnected");
        });

        socket.on('end',()=>{ 
            console.log("Client disconnected", socket.id);
            socket.disconnect(0)
        })

        const query = socket.handshake.query
        
        // Event 'transit-vehicles' to receive device vehicle data
        socket.on('transit-vehicles', async (data) => {
            
            if (data.hardware_serial) {

                
                const last_5_transit = await Transit.find({
                    hardware_serial: data.hardware_serial
                }).sort({creation_date: -1}).limit(5).select({creation_date: 1, payload_fields:1, velocity:1})
                
                Vehicle.findOne({ hardware_serial: data.hardware_serial },
                    ).populate('route')
                    .exec((err, vehicle) => {
                        if (err) return
                        if (!vehicle) return
                        
                        const transit = new Transit(data)
                        transit.save( (err, transitCreated) => {
                            if(err){
                                return res.status(500).json({
                                    ok: false,
                                    msg: `Can´t create transit vehicle data` ,
                                    err
                                })
                            }
                        })

                        // console.log('My Param',query.myParam)

                        io.emit(data.hardware_serial, {
                            last_5_transit,
                            hardware_serial: vehicle.hardware_serial,
                            placa: vehicle.placa,
                            velocity: data.velocity,
                            passengers: data.passengers,
                            route: {
                                name: vehicle.route.name,
                                fare: vehicle.route.fare,
                                status: vehicle.route.status,
                                color: vehicle.route.color
                            },
                            payload_fields: data.payload_fields,
                        })

                    })
            }

        })
        socket.emit('vehicles', () => { console.log('emit vehicles') })
    });
}
module.exports = { createSocket }
