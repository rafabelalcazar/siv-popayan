const mongoose = require('mongoose')

const Schema = mongoose.Schema

var vehicleSchema = new Schema({
    hardware_serial: { type: String, required: [true, 'El serial es requerido'], unique: [true, 'El serial debe ser único'] },
    placa: { type: String, required: [true, 'La placa es requerida'], unique: [true, 'La placa debe ser unica'] },
    route: { type: Schema.Types.ObjectId, ref: 'Route', required: [true, 'La ruta es requerida'] }
})

module.exports = mongoose.model('Vehicle', vehicleSchema)