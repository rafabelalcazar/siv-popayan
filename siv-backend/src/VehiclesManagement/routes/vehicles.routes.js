const { Router, response } = require("express");

// IMPORTS
const vehicleController = require('../controllers/vehicles.controller')

const router = Router()

// Methods for vehicles
router.post('/', [], vehicleController.create)
router.get('/', [], vehicleController.showAll)
router.get('/route/:routeId', [], vehicleController.showVehiclesByRoute)
router.get('/:id', [], vehicleController.getOne)
router.put('/:id', [], vehicleController.update)
// router.delete('/:id', vehicleController.delete)

module.exports = router