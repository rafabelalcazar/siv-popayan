const Vehicle = require('../models/vehicle.model')
// const bcrypt = require('bcryptjs')
// const jwt = require('jsonwebtoken')
const Transit = require('../../TransitCenterInformationServices/models/transit.model')
// const moment = require('moment')

const controller = {

    /**
     * Returns all vehicles registered in SIV.
     *
     * @param {Object} request http request.
     * @param {Object} response http response.
     * @return {Object} http response with vehicles.
     */
    showAll: async (req, res) => {
        const vehicles = await Vehicle.find({}).populate('route')

        if(!vehicles){
            return res.status(400).json({
                ok:false,
                msg: 'No vehicles found'
            })
        }

        res.status(200).json({
            ok: true,
            vehicles
        })

    },

    /**
     * Returns all vehicles related to a specific route.
     *
     * @param {Object} request http request.params must contains (routeId).
     * @param {Object} response http response.
     * @return {Object} http response with vehicles related to a specific route.
     */
    showVehiclesByRoute: (req, res) => {

        const routeId = req.params.routeId

        Vehicle.find({ route: routeId }, (err, vehicles) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    msg: 'Can´t get vehicles',
                    err
                })
            }
            res.status(200).json({
                ok: true,
                vehicles
            })
        })
    },

    /**
     * Returns one vehicle with a specific id.
     *
     * @param {Object} request http request.params must contains (id).
     * @param {Object} response http response.
     * @return {Object} http response with one vehicle and transit data if exists.
     */
    getOne: async (req, res) => {
        const id = req.params.id

        Vehicle.findById(id, async (err, vehicle) => {

            if (err) {
                return res.status(500).json({
                    ok: false,
                    msg: 'Can´t find the vehicle',
                    err
                })
            }

            if (!vehicle) {
                return res.status(400).json({
                    ok: false,
                    msg: `This vehicle with this id ${id} doesn´t exists`
                })
            }

            await Transit.findOne({ hardware_serial: vehicle.hardware_serial },
                {},
                { sort: { 'creation_date': 1 } }
                , (err, transit) => {

                    if (err) {
                        return res.status(500).json({
                            ok: false,
                            msg: 'Can´t find the transit register',
                            err
                        })
                    }

                    if (!transit) {
                        return res.status(400).json({
                            ok: false,
                            vehicle,
                            msg: `This transit register doesn´t exists`
                        })
                    }

                    res.status(200).json({
                        ok: true,
                        vehicle,
                        transit
                    })

                })
        })
    },

    /**
     * Create a vehicle.
     *
     * @param {Object} request http request.body must contains Vehicle Model Data.
     * @param {Object} response http response.
     * @return {Object} http response confirmation.
     */
    create: (req, res) => {

        const body = req.body

        const vehicle = new Vehicle({
            hardware_serial: body.hardware_serial,
            placa: body.placa,
            route: body.route
        })

        vehicle.save((err, vehicleCreated) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    msg: 'Can´t create vehicle',
                    err
                })
            }

            res.status(201).json({
                ok: true,
                msg: 'vehicle created',
                vehicleCreated,
                vehicleToken: req.vehicle
            })
        })
    },

    /**
     * update a vehicle.
     *
     * @param {Object} request http request.body must contains Vehicle Model Data.
     * @param {Object} response http response.
     * @return {Object} http response confirmation.
     */
    update: (req, res) => {

        const id = req.params.id
        body = req.body

        Vehicle.findById(id, (err, vehicle) => {

            if (err) {
                return res.status(500).json({
                    ok: false,
                    msg: 'Can´t find the vehicle',
                    err
                })
            }

            if (!vehicle) {
                return res.status(400).json({
                    ok: false,
                    msg: `This vehicle with this id ${id} doesn´t exists`
                })
            }

            vehicle.number = body.number
            vehicle.hardware_serial = body.hardware_serial
            vehicle.placa = body.placa

            vehicle.save((err, vehicleSaved) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        msg: `Error updating vehicle`,
                        err
                    })
                }

                res.status(200).json({
                    ok: true,
                    msg: 'vehicle updated',
                    vehicleSaved
                })
            })
        })
    },

    /**
     * delete a vehicle.
     *
     * @param {Object} request http request.params must contains (id).
     * @param {Object} response http response.
     * @return {Object} http response confirmation.
     */
    delete: (req, res) => {

        const id = req.params.id

        Vehicle.findByIdAndDelete(id, (err, vehicleDeleted) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    msg: 'Error deleting vehicle',
                    err
                })
            }
            if (!vehicleDeleted) {
                return res.status(400).json({
                    ok: false,
                    msg: `This vehicle with this id ${id} doesn´t exists `,
                    err
                })
            }

            res.status(200).json({
                ok: true,
                msg: 'vehicle deleted',
                vehicleDeleted
            })
        })
    }

}

module.exports = controller