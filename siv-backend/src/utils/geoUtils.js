/**
 * Returns distance between to points in km.
 *
 * @param {[number, number]} P1 [lat, lng] coords Point 1.
 * @param {[number,number]} P2 [lat, lng] coords Point 2.
 * @return {number} distance between P1 and P2 in Km.
 */
function getDistanceCoordsInKm(P1, P2) {
    lat1 = P1[0]
    lon1 = P1[1]
    
    lat2 = P2[0]
    lon2 = P2[1]

    const R = 6371; // Radius of the earth in km
    let dLat = deg2rad(lat2 - lat1);  // deg2rad below
    let dLon = deg2rad(lon2 - lon1);
    let a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c; // Distance in km
    return d;
}

/**
 * Converts from degrees to radians.
 *
 * @param {number} deg degrees.
 * @return {number} radians.
 */
function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

module.exports = {
    getDistanceCoordsInKm
}