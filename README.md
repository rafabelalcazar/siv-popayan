# Getting Started with "SISTEMA DE INFORMACION AL VIAJERO (SIV)".

This system is part of the "Prueba de Concepto Sistema de Información al Viajero (SIV) de transporte público, basado en servicios de Sistemas Inteligentes de Transporte (ITS)." project carried out at the Universidad del Cauca. 

This run this project you need to make sure you have installed nodejs. 

You can install it from here: [https://nodejs.org/en/](https://nodejs.org/en/).

## RUN SERVER

<i>Here you can find the server side code(Backend), responsible for responding to traveler requests.</i>

1. Navigate into the siv-backend folder.

`cd siv-backend`

2. Once inside the siv-backend folder, install all dependencies with the next command.

`npm install`

3. To run the server, run the following command.

`npm start`

## RUN CLIENT

<i>Here you can find the UI code(Frontend), created with reactjs library.</i> 

1. Navigate into the siv-client folder.

`cd siv-client`

2. Once inside the siv-client folder, install all dependencies with the next command.

`npm install`

3. To run the server, run the following command.

`npm start`

IMPORTANT: 
When you run the siv-client make sure what is the enpoint to connect to the server (production/local).

Edit .env file and switch between the next lines.

REACT_APP_ENDPOINT = 'https://siv-backend-popayan.herokuapp.com' : For production

REACT_APP_ENDPOINT = 'http://localhost:3030' : For development(local)


## RUN DATASETMANAGER

<i>With the datasetManager you can simulate vehicles movement from a dataset.</i>

1. Navigate into the datasetManager folder.

`cd datasetManager`

2. Once inside the datasetManager folder, install all dependencies with the next command.

`npm install`

3. To run the server, run the following command.

`npm start`

IMPORTANT: 
When you run the datasetManager make sure what is the enpoint used for the socket.

'https://siv-backend-popayan.herokuapp.com': For production

'http://localhost:3030' : For development(local)






