const { default: axios } = require("axios")
const csvtojson = require("csvtojson")
const moment = require("moment")
const cron = require("node-cron")
const { io } = require("socket.io-client")

// const socket = io('http://localhost:3030')
const socket = io('https://siv-backend-popayan.herokuapp.com')


socket.emit('vehicle', 'hola')
const mapDataset = () => {
    console.log("Hello")
    try {
        csvtojson().fromFile(__dirname + "/dataset_passengers.csv")
        // csvtojson().fromFile(__dirname + "/dataset_manual.csv")
            .then(csvData => {

                // Set date time to start simulation
                // let startAt = moment("2021-05-10T23:54:30-05:00", 'YYYY-MM-DD HH:mm:ss SSS')
                let startAt = moment()
                let firstRegisterDate
                let diff
                //En registro 5290, 6280 variedad de vehiculos (4) simultaneamente
                const startAtregister = 6280


                csvData.map((item, idx) => {
                    if(idx < startAtregister){
                        console.log(idx)
                        return
                    }
                    if (idx === startAtregister) {
                        firstRegisterDate = moment(item.creation_date, 'YYYY-MM-DD HH:mm:ss SSS')
                        diff = startAt.diff(firstRegisterDate)
                        console.log(firstRegisterDate,diff)

                    }

                    const registerDate = moment(item.creation_date, 'YYYY-MM-DD HH:mm:ss SSS')

                    const dateSimulation = registerDate.add(diff, "ms", true)

                    // Schedule simulation event
                    const month = dateSimulation.format('MM')
                    const day = dateSimulation.format('DD')
                    const hour = dateSimulation.format('HH')
                    const minute = dateSimulation.format('mm')
                    const second = dateSimulation.format('ss')


                    // Format to cron schedule: 'seconds minutes hours day month * '
                    cron.schedule(`${second} ${minute} ${hour} ${day} ${month} *`, () => {
                        console.log(`schedule event on ${dateSimulation}: ${JSON.stringify({
                            creation_date: dateSimulation,
                            hardware_serial: item.hardware_serial,
                            payload_fields: item.payload_fields,
                            velocity: item.velocity,
                            passengers: item.passengers,
                        })}`)

                        // Next block code is wotking
                        // socket.emit('transit-vehicles', {
                        //     creation_date: dateSimulation,
                        //     hardware_serial: item.hardware_serial,
                        //     payload_fields: item.payload_fields,
                        //     velocity: item.velocity,
                        //     passengers: item.passengers,
                        // })

                        // Next code is a experiment
                        socket.emit('transit-vehicles', {...item,
                            creation_date: dateSimulation,
                        })
                        //TODO: SIMULATE HTTP request
                        // axios.post('http://localhost', 

                        // })
                    })
                })
            })
            .catch(err => console.log(err))
    } catch (error) {
        console.log(error)

    }
}

mapDataset()