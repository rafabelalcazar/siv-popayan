import { useEffect, useMemo, useState } from "react";
import socketIOClient from "socket.io-client";

const useSocket = (url) => {

    const socket = useMemo(() => socketIOClient(url, {
        transports: ['websocket']
    }), [url])

    const [online, setOnline] = useState(false)

    useEffect(() => {
        setOnline(socket.connected)
    }, [socket])

    useEffect(() => {
        socket.on('connect', () => {
            setOnline(true)
            console.log('connected')
        })
        socket.on('disconnect', () => {
            console.log('disconnected')
            setOnline(false)
        })
        // socket.on('FromAPI',(data) => {console.log(data)})
    }, [socket])

    return { socket, online }
}

export default useSocket
