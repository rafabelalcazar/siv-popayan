import { useCallback, useEffect, useRef } from 'react'
// eslint-disable-next-line import/no-webpack-loader-syntax
import mapboxgl from 'mapbox-gl';

// mapboxgl.workerClass = require("worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker").default;
mapboxgl.accessToken = process.env.REACT_MAPBOX_ACCESS_TOKEN;

const useMapbox = (initialPoint,style='mapbox://styles/mapbox/streets-v11') => {

    const { lng = -76.613, lat = 2.44923, zoom = 12 } = initialPoint

    const mapDiv = useRef(null)
    const map = useRef(null)

    const setRef = useCallback((node) => {
        mapDiv.current = node
    }, [])


    useEffect(() => {
        if(map.current) return
        const newMap = new mapboxgl.Map({
            container: mapDiv.current,
            style: style,
            center: [lng, lat],
            zoom: zoom
        })
        map.current = newMap

        map.current.addControl(new mapboxgl.GeolocateControl({
            positionOptions: { enableHighAccuracy: true },
            trackUserLocation: true
        }))
        
    }, [initialPoint])

    return {
        setRef,
        map: map,
    }


}

export default useMapbox
