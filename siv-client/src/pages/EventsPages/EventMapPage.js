import React, { useState, useEffect, useMemo } from 'react';
import useMapbox from '../../hooks/useMapbox';
import { DateTimePicker } from '@material-ui/pickers';
import axios from 'axios';
import moment from 'moment';
import { TextField } from '@material-ui/core';
import { t_btn } from '../../constants/tailwindStyles';
import { Link } from 'react-router-dom';

const initialPoint = {
    lng: -76.6223,
    lat: 2.44812,
    zoom: 12
}

/**
* Render UI to show events by type from an initial date
*
*/
const EventMapPage = () => {

    const { setRef, map } = useMapbox(initialPoint)

    const [initialDate, setInitialDate] = useState()
    const [type, setType] = useState('Accidente');
    const [events, setEvents] = useState([])

    const getEvents = async () => {
        const url = `${process.env.REACT_APP_ENDPOINT}/api/events/period`
        // const url = `http://localhost:3030/api/events/period`
        const now = moment().add(-1, 'hours')
        const { data } = await axios.post(url, {
            initialDate: initialDate ? initialDate : now,
            type: type
        })
        const { events } = await data
        return events
    }

    const handleGetEvents = () => {
        getEvents().then((res) => {
            setEvents(res)
        })
    }

    const initialRoute = {
        'type': 'geojson',
        'data': {
            "type": "FeatureCollection",
            "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
            // "features": [
            //     { "type": "Feature", "properties": { "id": "ak16994521", "mag": 1, "time": 1507425650893, "felt": null, "tsunami": 0 }, "geometry": { "type": "Point", "coordinates": [-76.6223, 2.44812, 1] } },
            // ]
            "features": events.map(event => ({
                "type": "Feature", "properties": { "id": "ak16994521", "mag": 1, "time": 1507425650893, "felt": null, "tsunami": 0 }, "geometry": { "type": "Point", "coordinates": [event.location[1], event.location[0], 1] }
            }))
        }
    }

    useEffect(() => {
        if (!map?.current) return
        if (map.current.getSource('events')) {
            map.current.getSource('events').setData(initialRoute.data)
        }
    }, [events])

    useEffect(() => {
        if (!map?.current) return
        map.current.on('click', () => {
        })
        map.current.on('load', () => {
            getEvents().then((res) => {
                console.log(res);
            setEvents(res)
        })

            if (!map?.current?.getSource('events')) {
                map?.current?.addSource('events', initialRoute)


            }


            map.current.addLayer(
                {
                    'id': 'earthquakes-heat',
                    'type': 'heatmap',
                    'source': 'events',
                    'maxzoom': 20,
                    'paint': {
                        'heatmap-weight': [
                            'interpolate',
                            ['linear'],
                            ['get', 'mag'],
                            0,
                            0,
                            6,
                            1
                        ],
                        // Increase the heatmap color weight weight by zoom level
                        // heatmap-intensity is a multiplier on top of heatmap-weight
                        'heatmap-intensity': [
                            'interpolate',
                            ['linear'],
                            ['zoom'],
                            0,
                            1,
                            9,
                            20
                        ],
                        // Color ramp for heatmap.  Domain is 0 (low) to 1 (high).
                        // Begin color ramp at 0-stop with a 0-transparancy color
                        // to create a blur-like effect.
                        'heatmap-color': [
                            'interpolate',
                            ['linear'],
                            ['heatmap-density'],
                            0,
                            'rgba(33,102,172,0)',
                            0.2,
                            'rgb(103,169,207)',
                            0.4,
                            'rgb(209,229,240)',
                            0.6,
                            'rgb(253,219,199)',
                            0.8,
                            'rgb(239,138,98)',
                            1,
                            'rgb(178,24,43)'
                        ],
                        // Adjust the heatmap radius by zoom level
                        'heatmap-radius': [
                            'interpolate',
                            ['linear'],
                            ['zoom'],
                            0,
                            2,
                            9,
                            20
                        ],
                        // Transition from heatmap to circle layer by zoom level
                        'heatmap-opacity': [
                            'interpolate',
                            ['linear'],
                            ['zoom'],
                            0,
                            20,
                            0.8,
                            1
                        ]
                    }
                },
                'waterway-label'
            );

            map.current.addLayer(
                {
                    'id': 'earthquakes-point',
                    'type': 'circle',
                    'source': 'events',
                    'minzoom': 7,
                    'paint': {
                        // Size circle radius by earthquake magnitude and zoom level
                        'circle-radius': [
                            'interpolate',
                            ['linear'],
                            ['zoom'],
                            7,
                            ['interpolate', ['linear'], ['get', 'mag'], 1, 1, 6, 4],
                            16,
                            ['interpolate', ['linear'], ['get', 'mag'], 1, 5, 6, 50]
                        ],
                        // Color circle by earthquake magnitude
                        'circle-color': [
                            'interpolate',
                            ['linear'],
                            ['get', 'mag'],
                            1,
                            'rgba(33,102,172,0)',
                            2,
                            'rgb(103,169,207)',
                            3,
                            'rgb(209,229,240)',
                            4,
                            'rgb(253,219,199)',
                            5,
                            'rgb(239,138,98)',
                            6,
                            'rgb(178,24,43)'
                        ],
                        'circle-stroke-color': 'white',
                        'circle-stroke-width': 1,
                        // Transition from heatmap to circle layer by zoom level
                        'circle-opacity': [
                            'interpolate',
                            ['linear'],
                            ['zoom'],
                            7,
                            0,
                            8,
                            1
                        ]
                    }
                },
                'waterway-label'
            );

        })
        return
    }, [map, events])

    const hadleEventType = (e) => {
        setType(e.target.value)
    }

    const eventTypes = [
        {
            name: 'Accidente'
        },
        {
            name: 'Obra'
        },
        {
            name: 'Tráfico pesado'
        }
    ]

    return <div className="absolute top-16 right-0 left-0 bottom-32 bg-red-200">
        <div ref={setRef} className="w-full flex flex-1 h-full rounded-lg"  ></div>
        <div className="w-full bg-white bottom-0 fixed rounded-t-2xl p-2 md:p-6">
            <div className='w-full' >

                <DateTimePicker
                    className="w-full"
                    autoOk={true}
                    margin="normal"
                    id="date-picker-dialog"
                    label="Desde"
                    inputVariant="outlined"
                    variant="inline"
                    format="dd/MM/yyyy hh:mm a"
                    value={initialDate}
                    onChange={(e) => setInitialDate(e)}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                />
                <TextField
                    className="w-full"
                    name="type"
                    select
                    value={type}
                    label="Tipo"
                    variant="outlined"
                    defaultValue=''
                    SelectProps={{
                        native: true,
                    }}
                    onChange={hadleEventType}
                    helperText=""
                >
                    {
                        eventTypes.map(event => <option>{event.name}</option>)
                    }
                </TextField>
            </div>
            <div className=" absolute left-0  right-0 px-8" style={{top:'-50px'}}>
                <button type="button" className={`${t_btn} w-full`} onClick={handleGetEvents}>Buscar</button>
            </div>
            <div className="fixed top-20 left-4 md:hidden">
                    <Link to="/create-event">
                        <button
                            className="p-0 w-12 h-12 bg-blue-400 rounded-full hover:bg-blue-700 active:shadow-lg mouse shadow transition ease-in duration-200 focus:outline-none">
                            <svg viewBox="0 0 20 20" enableBackground="new 0 0 20 20" className="w-6 h-6 inline-block">
                                <path fill="#FFFFFF" d="M16,10c0,0.553-0.048,1-0.601,1H11v4.399C11,15.951,10.553,16,10,16c-0.553,0-1-0.049-1-0.601V11H4.601
                                    C4.049,11,4,10.553,4,10c0-0.553,0.049-1,0.601-1H9V4.601C9,4.048,9.447,4,10,4c0.553,0,1,0.048,1,0.601V9h4.399
                                    C15.952,9,16,9.447,16,10z" />
                            </svg>
                        </button>
                    </Link>
                </div>
                <div className="fixed top-20 left-4">
                    <Link to="/create-event">
                        <button
                            className="hidden md:block text-white px-4 w-auto h-10 bg-blue-400 rounded-full hover:bg-blue-700 active:shadow-lg mouse shadow transition ease-in duration-200 focus:outline-none">
                            <svg viewBox="0 0 20 20" enableBackground="new 0 0 20 20" className="w-6 h-6 inline-block">
                                <path fill="#FFFFFF" d="M16,10c0,0.553-0.048,1-0.601,1H11v4.399C11,15.951,10.553,16,10,16c-0.553,0-1-0.049-1-0.601V11H4.601
                                    C4.049,11,4,10.553,4,10c0-0.553,0.049-1,0.601-1H9V4.601C9,4.048,9.447,4,10,4c0.553,0,1,0.048,1,0.601V9h4.399
                                    C15.952,9,16,9.447,16,10z" />
                            </svg>
                            <span className='mx-24' >Reportar eventualidad</span>
                        </button>
                    </Link>
                </div>
        </div>
    </div>;
};

export default EventMapPage;
