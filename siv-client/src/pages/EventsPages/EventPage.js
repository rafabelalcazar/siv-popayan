import { Button, Snackbar, TextField, Typography } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import useMapbox from '../../hooks/useMapbox'
import mapboxgl from 'mapbox-gl'
import { t_btn } from '../../constants/tailwindStyles'
import axios from 'axios'
import Alert from '../../components/Alert'
const initialPoint = {
    lng: -76.613,
    lat: 2.4563,
    zoom: 12
}
mapboxgl.accessToken = 'pk.eyJ1IjoicmFmYWJlbGFsY2F6YXIiLCJhIjoiY2tteGo0cmk3MDZxZjJ2bXUwMHRyOWhmOSJ9.54HQH5wF2poXqTlRcTpRAA';

/**
* Render UI report an event
*
*/
const EventPage = () => {

    const { setRef, map } = useMapbox(initialPoint)

    const [event, setEvent] = useState("")
    const [coords, setCoords] = useState([0, 0])

    const [openSuccess, setOpenSuccess] = useState(false)
    const [openError, setOpenError] = useState(false)

    useEffect(() => {
        if (!map.current) return

        let startElement = document.createElement('div')
        startElement.innerHTML = `<h1>A</h1>`
        startElement.className = 'startPointMarker'

        const { lng, lat } = map.current.getCenter()
        const markerStart = new mapboxgl.Marker(startElement).setLngLat([lat, lng]).addTo(map.current)
        map?.current?.on('move', () => {
            const { lng, lat } = map.current.getCenter()
            markerStart.setLngLat([lng, lat])
            setCoords([lat, lng])
        })
        return () => {
            console.log('remove')
            // marker.remove()
        }


    }, [map])

    const handleChange = (e) => {
        console.log(e.target.name, e.target.value)
        setEvent(e.target.value)
    }

    const handleReportEvent = () => {

        const createEvent = async (e) => {
            const url = `${process.env.REACT_APP_ENDPOINT}/api/events/`;
            try {
                const { data } = await axios.post(url, {
                    type: event,
                    location: coords
                })
                setOpenSuccess(true)
            } catch (error) {
                setOpenError(true)
            }
        }

        createEvent()

    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSuccess(false);
    }

    const handleCloseError = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
        setOpenError(false);
      }

    const events = [
        {
            name: 'Accidente'
        },
        {
            name: 'Obra'
        },
        {
            name: 'Tráfico pesado'
        }
    ]
    return (
        <div className='h-full w-full' >
            <h3 className='my-4 font-bold text-center' >Eliga el tipo de eventualidad en la vía</h3>
            <div className='flex my-4'>
                <TextField
                    className="w-full"
                    name="event"
                    select
                    value={event}
                    label="Evento"
                    variant="outlined"
                    defaultValue=''
                    SelectProps={{
                        native: true,
                    }}
                    onChange={handleChange}
                    helperText=""
                >
                    <option></option>
                    {
                        events.map(event => <option>{event.name}</option>)
                    }

                </TextField>
            </div>
            <div className="relative rounded" style={{ height: '500px' }} ref={setRef}>
            </div>
            <div className="flex justify-center">
                <button className={`${t_btn}  btn my-4 mb-8`} onClick={handleReportEvent}  >Reportar eventualidad</button>

            </div>
            <Snackbar open={openSuccess} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success">
                    Reporte creado correctamente
                </Alert>
            </Snackbar>
            <Snackbar open={openError} autoHideDuration={6000} onClose={handleCloseError}>
                <Alert onClose={handleCloseError} severity="error">
                    Error al reportar eventualidad.
                </Alert>
            </Snackbar>


        </div>
    )
}

export default EventPage
