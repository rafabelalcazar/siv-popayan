import { Card, CardContent, Divider, TextField, Snackbar } from '@material-ui/core'
import { createUserWithEmailAndPassword } from 'firebase/auth'
import { getFirestore, doc, setDoc } from 'firebase/firestore'
import React from 'react'
import { useState } from 'react'
import { Link } from 'react-router-dom'
import { t_btn } from '../../constants/tailwindStyles'
import { app, auth } from '../../firebaseConfig'
import Alert from '../../components/Alert';

const firestore = getFirestore(app)

/**
* Render UI for register form
*/
const Register = () => {
    
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [openAlert, setOpenAlert] = useState(false);

    const register = async () => {
        try {
            const user = await createUserWithEmailAndPassword(
                auth,
                email,
                password
            )

            const docRef = await doc(firestore, `/users/${user.user.uid}`)
            setDoc(docRef, {
                mail: user.user.email,
                role: 'TRAVELER'
            })
        } catch (error) {
            console.log(error);
            setOpenAlert(true);
        }
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenAlert(false);
    }

    return (
        <div className=" h-full items-center justify-center w-full bg-red-20 mt-16">

            <div className='text-center' >
                <h2 className="font-bold">SIV Popayán</h2>
            </div>

            <Card className='md:mx-24 my-4' >
                <CardContent>
                    <h3 className=" font-bold my-1">Crea tu cuenta</h3>
                    <Divider />
                    <form className="card text-center items-center flex flex-col mx-24">
                        <div className='my-4 w-full md:w-80' >
                            <TextField
                                className="w-full"
                                label="Correo"
                                name="email"
                                type='email'
                                variant="outlined"
                                onChange={(e) => { setEmail(e.target.value) }}
                                value={email}
                            />
                        </div>
                        <div className='my-4 w-full md:w-80' >
                            <TextField
                                className="w-full"
                                label="Contraseña"
                                name="password"
                                inputProps={{minLength: 6}}
                                variant="outlined"
                                type='password'
                                onChange={(e) => { setPassword(e.target.value) }}
                                value={password}
                            />
                        </div>
                        <button
                            onClick={register}
                            type="button"
                            className={`${t_btn} w-full`}>
                            Continuar
                        </button>
                    </form>
                </CardContent>
            </Card>
            <div className="text-center my-4">
                <p>¿Ya tienes cuenta? Inicia sesión <Link className="text-blue-500" to='/login' >aquí</Link> </p>
            </div>
            {/* <p>
                {
                    auth?.currentUser?.email
                }

            </p> */}
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity='error'>
                    {
                        password.length >= 6 ? 
                        'El correo ya ha sido registrado previamente en la base de datos, por favor intente nuevamente.'
                        : 'La contraseña debe tener almenos 6 caracteres'
                    }
                    
                </Alert>
            </Snackbar>
        </div>
    )
}

export default Register
