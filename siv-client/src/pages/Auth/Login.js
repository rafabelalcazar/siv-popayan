import React from 'react'
import * as Actions from '../../store/actions/main.actions'
import { useState } from 'react'
import { t_btn } from '../../constants/tailwindStyles'
import { app, auth } from '../../firebaseConfig'
import { Card, CardContent, Divider, TextField, Snackbar } from '@material-ui/core'
import Alert from '../../components/Alert';
import { signInWithEmailAndPassword, sendPasswordResetEmail } from 'firebase/auth'
import { getFirestore, doc, getDoc } from 'firebase/firestore'
import { Link } from 'react-router-dom'
import { useHistory } from 'react-router-dom'
import { connect, useDispatch } from 'react-redux'

const firestore = getFirestore(app)

/**
* Render UI for login form
*/
const Login = ({main}) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [openAlert, setOpenAlert] = useState(false);
    const history = useHistory()

    const login = async () => {
        try {
            const userCredentials = await signInWithEmailAndPassword(auth, email, password)
            history.push('/routes')
        } catch (error) {
            console.log(error);
            setOpenAlert(true);
        }
    }

    const restorePassword = () => {
        sendPasswordResetEmail(auth, email)
        .then(() => {
            alert("Te hemos enviado un correo, mira tu bandeja de entrada y sigue las instrucciones")
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log(errorCode)
            console.log(errorMessage)
            // ..
        });
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenAlert(false);
    }

    return (
        <div className=" h-full items-center justify-center w-full bg-red-20 mt-16">
            <div className='text-center' >
                <h2 className="font-bold">SIV Popayán</h2>
            </div>

            <Card className='md:mx-24 my-4' >
                <CardContent>
                    <h3 className=" font-bold my-1">Iniciar sesión</h3>
                    <Divider />
                    <form className="card text-center items-center flex flex-col mx-24">
                        <div className='my-4 w-full md:w-80' >
                            <TextField
                                className="w-full"
                                label="Correo"
                                name="email"
                                type='email'
                                variant="outlined"
                                onChange={(e) => { setEmail(e.target.value) }}
                                value={email}
                            />
                        </div>
                        <div className='my-4 w-full md:w-80' >
                            <TextField
                                className="w-full"
                                label="Contraseña"
                                name="password"
                                variant="outlined"
                                type='password'
                                onChange={(e) => { setPassword(e.target.value) }}
                                value={password}
                            />
                        </div>
                        <button
                            onClick={login}
                            type="button"
                            className={`${t_btn} w-full`}>
                            Inicia sesión
                        </button>
                    </form>
                </CardContent>
            </Card>
            <div className="text-center my-4">
                <p>¿No tienes cuenta? <Link className="text-blue-500" to='/register' >Registrate</Link> </p>
            </div>
            <div className="text-center my-4">
                <button onClick={restorePassword}>Olvidaste tu contrasena?</button>
            </div>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity='error'>
                    Credenciales incorrectas, por favor intente nuevamente.
                </Alert>
            </Snackbar>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        main: state.main
    }
}

export default connect(mapStateToProps, null)(Login)
