import { signOut } from 'firebase/auth'
import React from 'react'
import { t_btn_danger } from '../../constants/tailwindStyles'
import { auth } from '../../firebaseConfig'

/**
* Render UI to show user data and allow logout
*/
const MyAccount = () => {

    const logout = () => {
        if (auth?.currentUser?.email) {
            signOut(auth)
            console.log('cerrar sesi[on')
        }
    }

    return (
        <div className=" h-full items-center justify-center w-full bg-red-20 mt-16">
            {
                auth?.currentUser?.email ?
                    <div className='h-full' >
                        <h3 className='font-bold' >Mi Cuenta</h3>
                        <div className='h-full'>
                            <p>Email: <span>{auth?.currentUser?.email}</span> </p>
                        </div>

                        {/* <p>{auth?.currentUser?.displayName}</p> */}
                        <div className="bg-red-200">
                            <button
                                onClick={logout}
                                type="button"
                                className={`${t_btn_danger} w-full`}>
                                Cerrar sesión
                            </button>
                        </div>
                    </div> :
                    <div>
                        <p>Aun no has iniciado sesión</p>
                    </div>
            }
        </div>
    )
}

export default MyAccount
