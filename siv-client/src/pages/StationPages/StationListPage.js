import React, { useEffect, useState } from 'react';
import { Card, Typography } from '@material-ui/core';
import axios from 'axios'
import { Link } from 'react-router-dom';

/**
* Render UI to show a list with all station registered
*/
const StationListPage = () => {

    const [stations, setStations] = useState([]);

    /**
    * Request to get all stations
    */
    useEffect(() => {
        const url = `${process.env.REACT_APP_ENDPOINT}/api/stations/`;
        axios.get(url).then(({ data }) => {
            if (!!data?.stations) {
                setStations(data.stations)
            }
        })
    }, [])

    return <div className=' h-full' >
        <div className='mt-4' >
            <h3 className='font-bold my-8 text-center'>Estaciones de parada SIV Popayán</h3>
            {
                stations.map(st => (
                    <Link to={`stations/${st._id}`} >
                        <Card className='p-4 my-4  flex justify-between flex-wrap' onClick={(e) => { console.log(e) }} >
                            <h4 className='p-2 font-bold'>
                                {st.name}
                            </h4>
                            <h4 className='p-2 bg-blue-600 font-bold rounded text-white'>
                                {`Coordenadas: [${st.location[0]}, ${st.location[1]}]`}
                            </h4>
                        </Card>
                    </Link>
                ))
            }
        </div>
    </div>;
};

export default StationListPage;
