import { Divider, Icon, Slide, Typography } from '@material-ui/core'
import { DirectionsBus } from '@material-ui/icons'
import React, { useContext, useEffect, useState, useMemo } from 'react'
import useMapbox from '../../hooks/useMapbox'
import mapboxgl from 'mapbox-gl'
import './station.css'
import { SocketContext } from '../../context/SocketContext'
import { calculateBearing, getDistanceFromLatLonInKm, getNearPointInTrajectory, getNearRoutes, getNearRoutesTrajectory } from '../../utils/getDistanceCoords'
import { getLatLngFromPayload } from '../../utils/utils'
import moment from 'moment'
import axios from 'axios'
import { useParams } from 'react-router'
mapboxgl.accessToken = 'pk.eyJ1IjoicmFmYWJlbGFsY2F6YXIiLCJhIjoiY2tteGo0cmk3MDZxZjJ2bXUwMHRyOWhmOSJ9.54HQH5wF2poXqTlRcTpRAA';

const defaultPoint = {
    lng: -76.9,
    lat: 2.45120,
    zoom: 15
}

/**
* Render UI to show a station on map and the routes and vehicles near the current station
*/
const StationPage = () => {

    const { socket } = useContext(SocketContext)

    let { id } = useParams();
    const [station, setStation] = useState({})
    const [initialPoint, setInitialPoint] = useState(defaultPoint)
    const { setRef, map } = useMapbox(defaultPoint)
    const [routeList, setRouteList] = useState([])
    const [routesResult, setRoutesResult] = useState([])
    const [indexRouteSelected, setIndexRouteSelected] = useState(0)
    const [vehiclesRichData, setVehiclesRichData] = useState([])
    const [vehicles, setVehicles] = useState([])
    const [markers, setMarkers] = useState({})


    /**
    * Request to get station data by id
    */
    useEffect(() => {

        if (!id) return
        if (!map.current) return
        const getStation = async () => {
            const url = `${process.env.REACT_APP_ENDPOINT}/api/stations/${id}`;
            const { data } = await axios.get(url)
            setStation(data.station)
            setInitialPoint({
                lng: data.station.location[1],
                lat: data.station.location[0],
                zoom: 15
            })

            let el = document.createElement('div')
            el.className = 'stopStation'
            const mark = new mapboxgl.Marker(el).setLngLat([data.station.location[1], data.station.location[0]]).addTo(map.current)
            map.current.flyTo({
                center: [data.station.location[1], data.station.location[0]]
            })

        }

        getStation()
        return () => {
            // cleanup
        }
    }, [id, map])

    // Search for near routes
    useEffect(() => {
        const searchNearRoutes = getNearRoutes(routeList, [initialPoint.lat, initialPoint.lng], 0.1)
        setRoutesResult(searchNearRoutes)
    }, [routeList])

    // get async routeList
    useEffect(() => {

        const url = `${process.env.REACT_APP_ENDPOINT}/api/routes/`;

        fetch(url)
            .then((resp) => resp.json())
            .then(routes => {
                setRouteList(routes.routes)
            })
        return () => {
            console.log('Bye List routes')
        }
    }, [])

    console.log(indexRouteSelected)

    // Timer to set route selected every 15 seconds
    // get vehicles from routes
    useEffect(() => {
        let timer = setTimeout(() => {
            if (routesResult.length && indexRouteSelected < routesResult.length - 1) {
                setIndexRouteSelected(indexRouteSelected + 1)
            } else {
                setIndexRouteSelected(0)
            }
        }, 15000)

        return () => {
            clearTimeout(timer)
        }
    }, [routesResult, indexRouteSelected])

    // Draw path route selected
    useEffect(() => {
        if (!map.current) return
        if (!routesResult.length) return

        const coords = routesResult[indexRouteSelected].path.map(p => ([p.lng, p.lat]))
        if (!coords.length) return
        const initialRoute = {
            'type': 'FeatureCollection',
            'features': [
                {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': routesResult[indexRouteSelected]?.path.map(item => ([item.lng, item.lat]))
                    }
                }
            ]
        }
        if (!map.current.getSource('route')) {
            map.current?.addSource('route', { type: 'geojson', data: initialRoute })
        }
        if (map.current.getSource('route')) {
            map.current.removeLayer('route')
            console.log('source already exist')
            map.current.getSource('route').setData(initialRoute)

            map.current.addLayer({
                id: 'route',
                type: 'line',
                source: 'route',
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': routesResult[indexRouteSelected].color || '#03AA46',
                    'line-width': 8,
                    'line-opacity': 0.8
                }
            })
            getVehicles()
            return
        }

    }, [indexRouteSelected, map])


    /**
    * Subscribe to every hardware serial event(vehicle)
    */
    useEffect(() => {

        vehicles?.map(vehicle => {
            socket?.on(vehicle.hardware_serial, data => {
                updateMarkers(data)
            })
        })

        return () => {
            // console.log('RETURN USEEFFECT VEHICLES', vehicles)
            vehicles?.map(vehicle => {
                socket.removeListener(vehicle.hardware_serial, data => { console.log(data) })
                socket.off()
                setMarkers({})
            })
        }
    }, [vehicles])

    /**
    * Caculate (avgVelocity, timeToArrive, distanceToArrive) from vehicles data
    */
    useEffect(() => {
        const h_serials = Object.keys(markers)
        const arrayMarkers = []

        const vehiclesRichData = h_serials.map(serial => {
            
            // Calculate time to arrive to A point
            const nearPointToStart = getNearPointInTrajectory(routesResult[indexRouteSelected], [initialPoint.lat, initialPoint.lng])
            const nearPointVehicle = getNearPointInTrajectory(routesResult[indexRouteSelected], [markers[serial].lat, markers[serial].lng])

            let distanceToArrive = 0

            const fullPath = routesResult[indexRouteSelected].path.reduce((acc, current, idx) => {
                const distance = acc + (getDistanceFromLatLonInKm([current.lat, current.lng], [routesResult[indexRouteSelected].path[idx + 1]?.lat, routesResult[indexRouteSelected].path[idx + 1]?.lng]) || 0)
                return distance
            }, 0)
            if (nearPointToStart > nearPointVehicle) {

                const semiPath = routesResult[indexRouteSelected].path.slice(nearPointVehicle, nearPointToStart)

                distanceToArrive = semiPath.reduce((acc, current, idx) => {
                    const distance = acc + (getDistanceFromLatLonInKm([current.lat, current.lng], [semiPath[idx + 1]?.lat, semiPath[idx + 1]?.lng]) || 0)
                    return distance
                }, 0)
            } else if (nearPointVehicle > nearPointToStart) {
                const semiPathToLastPointRoute = routesResult[indexRouteSelected].path.slice(nearPointVehicle, routesResult[indexRouteSelected].path.length - 1)
                const semiPathFromFirstPointRoute = routesResult[indexRouteSelected].path.slice(0, nearPointToStart)

                const semiPath = [...semiPathToLastPointRoute, semiPathFromFirstPointRoute]

                distanceToArrive = semiPath.reduce((acc, current, idx) => {
                    const distance = acc + (getDistanceFromLatLonInKm([current.lat, current.lng], [semiPath[idx + 1]?.lat, semiPath[idx + 1]?.lng]) || 0)
                    return distance
                }, 0)
            }

            const sumVelocity = markers[serial].last_5_transit.reduce((acc, current) => acc + Number(current.velocity), 0)
            const avgVelocity = sumVelocity / 5
            const timeToArrive = (distanceToArrive / avgVelocity) * 60
            const timaToArriveFormat = `${moment.duration(timeToArrive, 'minutes').minutes()}`

            let el = document.createElement('div')
            el.innerHTML = `<h1>${markers[serial]?.route?.name}</h1>`
            el.className = 'vehicleMarker'
            // el.style.backgroundColor = `${markers[serial]?.route?.color}`
            const mark = new mapboxgl.Marker(el).setLngLat([markers[serial].lng, markers[serial].lat])
                .setPopup(new mapboxgl.Popup({ offset: 25, className: '' })
                    .setHTML(`<b>Serial: ${serial}</b>
            <p>Velocidad: ${Number.parseFloat(markers[serial]['velocity']).toFixed(2)} Km/hr</p>
            <p>Pasajeros: ${markers[serial]['passengers']}</p>
            <p>Tiempo de llegada: ${timaToArriveFormat} minutos</p>
            <p>Distancia al vehiculo: ${distanceToArrive?.toFixed(2)} Km</p>
            `
                    ))
                .addTo(map.current)
            const { lat: previusLat, lng: previusLng } = getLatLngFromPayload(markers[serial].last_5_transit[1]['payload_fields'])
            const bearing = calculateBearing(previusLat, previusLng, markers[serial].lat, markers[serial].lng)
            // console.log('bearing', bearing)
            el.style.transform = el.style.transform + `rotate(${bearing}deg)`
            arrayMarkers.push(mark)

            return {
                avgVelocity,
                timeToArrive,
                distanceToArrive,
                fullPath,
                ...markers[serial]
            }
        })
        setVehiclesRichData(vehiclesRichData)
        return () => arrayMarkers.map(m => m.remove())
    }, [markers])

    const updateMarkers = (dataVehicle) => {

        const { payload_fields, ...rest } = dataVehicle

        const { lat, lng } = getLatLngFromPayload(payload_fields)
        const newMarkers = markers
        newMarkers[dataVehicle.hardware_serial] = { lng: Number(lng), lat: Number(lat), route: dataVehicle.route, ...rest }
        setMarkers(currentMarkers => ({ ...currentMarkers, ...newMarkers }))
    }

    const getVehicles = () => {
        const url = `${process.env.REACT_APP_ENDPOINT}/api/vehicles/route/`;
        fetch(url + routesResult[indexRouteSelected]._id)
            .then((resp) => resp.json())
            .then(vehicles => {
                // console.log('vehicles', vehicles)
                setVehicles(vehicles.vehicles)
                // setRouteList(vehiles.routes)
            }).catch((err) => { console.log(err) });
    }

    console.log(station)

    return (
        <div className='flex  h-full absolute top-0 right-0 left-0 bottom-0 md:pt-16'>
            {
                <>
                    <div className='bg-red-200 w-1/2 h-full ' >
                        <div ref={setRef} className="w-full flex flex-1 h-full rounded-lg" />

                    </div>
                    <div className=' w-1/2 h-full shadow-lg p-8' >
                        <h3 className='text-center font-bold m-4' >Bienvenido a SIV Popayán</h3>
                        <Typography className='text-center'>{station.name ? station.name : 'Está estación no existe'}</Typography>
                        <div className='h-1/2' >
                            <h4 className='text-center font-bold m-4' >Rutas</h4>
                            <div className='flex' >

                                {
                                    routesResult.map((route, idx) => (
                                        <button key={idx} className={`flex text-4xl bg-blue-500 rounded p-8 w-20 h-20 text-white font-bold m-4 items-center justify-center`} style={{ backgroundColor: `${idx === indexRouteSelected ? route.color : '#999999'}` }} >
                                            {route.name}
                                        </button>
                                    ))
                                }
                            </div>
                        </div>
                        <Divider />
                        <div className='h-1/2'>
                            <h4 className='text-center font-bold m-4' >Vehiculos</h4>
                            {
                                vehiclesRichData?.map(vehicle => (
                                    <Slide direction="up" in={true} >

                                        <button className="flex text-white rounded h-36 shadow hover:shadow-md bg-white overflow-hidden w-full">
                                            <div className="bg-blue-600 h-36 w-36 flex items-center justify-center ">
                                                <DirectionsBus className='h-full w-36' style={{ fontSize: 80 }} />
                                            </div>
                                            <div className='text-gray-700 flex p-4 h-full w-full flex-col h-40 items-start' >
                                                <Typography>Tiempo de llegada: {`${moment.duration(vehicle.timeToArrive, 'minutes').minutes()} minutos`} </Typography>
                                                <Typography>Pasajeros: {vehicle?.passengers}</Typography>
                                                <Typography>Velocidad: {Number(vehicle?.velocity)?.toFixed(2)} Km/h</Typography>
                                                <Typography>Velocidad prom: {Number(vehicle?.avgVelocity)?.toFixed(2)} Km/h</Typography>
                                            </div>
                                        </button>
                                    </Slide>
                                ))
                            }
                        </div>
                    </div>
                </>
            }
        </div>
    )
}

export default StationPage
