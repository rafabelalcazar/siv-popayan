import './userTrip.css'
import React, { useCallback, useContext, useEffect, useState } from 'react'
import useMapbox from '../../hooks/useMapbox'
import { t_btn } from '../../constants/tailwindStyles'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router';

import * as Actions from '../../store/actions'
import { createMarker } from '../../utils/mapboxHelpers'

const initialPoint = {
    lng: -76.613,
    lat: 2.4563,
    zoom: 12
}

/**
* Render UI to set the destination point of a trip
*
*/
const StartTripToPoint = () => {

    const history = useHistory()
    const { setRef, map } = useMapbox(initialPoint)

    const trip = useSelector(({ trip }) => trip.trip)

    const dispatch = useDispatch()

    /**
    * Render mapbox map and marker
    *
    */
    useEffect(() => {
        if (!map.current) return

        let startElement = document.createElement('div')
        startElement.innerHTML = `<h1>A</h1>`
        startElement.className = 'startPointMarker'
        const markerStart = createMarker({
            options: startElement,
            position: [trip.from.lat || 0, trip.from.lng || 0],
            map: map.current
        }) 

        let destinationElement = document.createElement('div')
        destinationElement.innerHTML = `<h1>B</h1>`
        destinationElement.className = 'endPointMarker'
        const { lng, lat } = map.current.getCenter()
        if(!lng && !lat) return
        const markerDestination = createMarker({
            options: destinationElement,
            position: [lat, lng],
            map: map.current
        }) 
        map?.current?.on('move', () => {
            const { lng, lat } = map.current.getCenter()
            // if (currentSet === 'done') return
            markerStart.setLngLat([trip.from.lat, trip.from.lng])
            markerDestination.setLngLat([lng, lat])
     
        })
        return () => {
            console.log('remove')
        }
    }, [map, trip])

    const handleStartPointChange = () => {
        console.log(map.current.getCenter())
        const { lng, lat } = map.current.getCenter()
        if(!!lng && !!lat) dispatch(Actions.setStartTripTo({ lat: lat, lng: lng }))

        history.push('/resumeTrip')
    }

    return (
        <div className='absolute top-0 right-0 left-0 bottom-0 pt-16'>
            <div ref={setRef} className="w-full flex flex-1 h-full rounded-lg" />
            <div className='fixed z-10 right-2 bottom-2 bg-red-200' >
            </div>
            <div className='w-full h-32 bg-white fixed bottom-0 shadow-2xl rounded-t-2xl p-8'>
                <div className='absolute right-0 left-0 px-8' style={{ top: '-50px' }} >
                    <button type="button" className={`${t_btn} w-full`} onClick={handleStartPointChange}  >
                        Fijar Punto de Destino
                    </button>
                </div>
                <div>
                    <p>Mueve el mapa para seleccionar tu punto de Destino.</p>
                    <p>usa el boton de la esquina superior derecha para detectar tu ubicación</p>
                </div>
            </div>
        </div>
    )
}

export default StartTripToPoint
