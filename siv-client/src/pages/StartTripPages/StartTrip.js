import './userTrip.css'
import React, { useCallback, useContext, useEffect, useState } from 'react'
import useMapbox from '../../hooks/useMapbox'
import mapboxgl from 'mapbox-gl'
import { t_btn } from '../../constants/tailwindStyles'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router';

import * as Actions from '../../store/actions'

mapboxgl.accessToken = 'pk.eyJ1IjoicmFmYWJlbGFsY2F6YXIiLCJhIjoiY2tteGo0cmk3MDZxZjJ2bXUwMHRyOWhmOSJ9.54HQH5wF2poXqTlRcTpRAA';


const initialPoint = {
    lng: -76.613,
    lat: 2.4563,
    zoom: 12
}

/**
* Render UI to set the starting point of a trip
*
*/
const StartTrip = () => {

    const history = useHistory()
    const { setRef, map } = useMapbox(initialPoint)

    const dispatch = useDispatch()

    /**
    * Render mapbox map and marker
    *
    */
    useEffect(() => {
        if (!map.current) return

        console.log('crear marcador inicio')
        let startElement = document.createElement('div')
        startElement.innerHTML = `<h1>A</h1>`
        startElement.className = 'startPointMarker'


        const { lng, lat } = map.current.getCenter()
        const markerStart = new mapboxgl.Marker(startElement).setLngLat([lat, lng]).addTo(map.current)
        map?.current?.on('move', () => {
            const { lng, lat } = map.current.getCenter()
            // if (currentSet === 'done') return
            markerStart.setLngLat([lng, lat])

        })
        return () => {
            // console.log('remove')
            // marker.remove()
        }


    }, [map])



    const handleStartPointChange = () => {
        // console.log(map.current.getCenter())
        const { lng, lat } = map.current.getCenter()
        dispatch(Actions.setStartTripFrom({ lat: lat, lng: lng }))
        history.push('/startTripTo')

    }

    return (
        <div className='absolute top-0 right-0 left-0 bottom-0 pt-16'>
            <div ref={setRef} className="w-full flex flex-1 h-full rounded-lg" />
            <div className='fixed z-10 right-2 bottom-2 bg-red-200' >

            </div>
            <div className='w-full h-32 bg-white fixed bottom-0 shadow-2xl rounded-t-2xl p-8'>
                <div className='absolute right-0 left-0 px-8' style={{ top: '-50px' }} >
                    <button type="button" className={`${t_btn} w-full`} onClick={handleStartPointChange}  >
                        Fijar Punto de Inicio
                    </button>
                    
                </div>

                <div>
                    <p>Mueve el mapa para seleccionar tu punto de Inicio.</p>
                    <p>usa el boton de la esquina superior derecha para detectar tu ubicación</p>
                </div>
            </div>
        </div>
    )
}

export default StartTrip
