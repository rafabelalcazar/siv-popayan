import React, { useMemo, useCallback, useContext, useEffect, useState } from 'react'
import './userTrip.css'
import useMapbox from '../../hooks/useMapbox'
import mapboxgl from 'mapbox-gl'
import { Accordion, AccordionDetails, AccordionSummary, Divider, Slide, Snackbar, TextField, Typography } from '@material-ui/core'
import { t_btn } from '../../constants/tailwindStyles'
import { useDispatch, useSelector } from 'react-redux'
import { calculateBearing, getCenterFromPath, getDistanceFromLatLonInKm, getNearPointInTrajectory, getNearRoutes, getNearRoutesTrajectory, getZoomValue } from '../../utils/getDistanceCoords'
import { getLatLngFromPayload } from '../../utils/utils'
import moment from 'moment'
import { createMarker } from '../../utils/mapboxHelpers'
import { ExpandMore, KeyboardArrowUp } from '@material-ui/icons'
import Alert from '../../components/Alert'
import useSocket from '../../hooks/useSocket'

mapboxgl.accessToken = 'pk.eyJ1IjoicmFmYWJlbGFsY2F6YXIiLCJhIjoiY2tteGo0cmk3MDZxZjJ2bXUwMHRyOWhmOSJ9.54HQH5wF2poXqTlRcTpRAA';

const initialPoint = {
    lng: -76.613,
    lat: 2.4563,
    zoom: 12
}

/**
* Render UI to show pre trip 
*/
const ResumeTrip = () => {
    const {socket, online, setParams} = useSocket(process.env.REACT_APP_ENDPOINT,{routeSelected: null, myParam: 'Hola'})

    const url = `${process.env.REACT_APP_ENDPOINT}/api/routes/`;
    const { setRef, map } = useMapbox(initialPoint)
    const trip = useSelector(({ trip }) => trip.trip)
    const [markers, setMarkers] = useState({})
    const [vehiclesRichData, setVehiclesRichData] = useState([])
    const [pointsInRoute, setPointsInRoute] = useState({
        nearFrom: [],
        nearTo: []
    })

    const [showResultSection, setShowResultSection] = useState(true)
    const [expanded, setExpanded] = useState(false)

    const [routeList, setRouteList] = useState([])
    const [routesResult, setRoutesResult] = useState([])
    const [routeSelected, setRouteSelected] = useState({
        status: '',
        color: '',
        fare: '',
        path: [],
        _id: '',
        name: '',
        nearPointFrom: '',
        nearPointTo: ''
    })
    const [vehicles, setVehicles] = useState([])

    const [openWarning, setOpenWarning] = useState({
        open: false,
        type: 'warning',
        message: "No se encontraron rutas para este viaje"
    })

    const initialRoute = useMemo(() => ({
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': routeSelected?.path?.map(item => ([item.lng, item.lat]))
                }
            }
        ]
    }), [routeSelected])

    useEffect(() => {
        console.log('route selected', routeSelected._id)
        setParams(params => ({...params, routeSelected: routeSelected._id}))
    },[routeSelected])

    /**
    * Request to get existing routes 
    *
    */
    useEffect(() => {

        fetch(url)
            .then((resp) => resp.json())
            .then(routes => {
                setRouteList(routes.routes)
            })
        return () => {
        }
    }, [])

    /**
    * Render mapbox marker (startPoint, endPoint)
    */
    useEffect(() => {
        if (!map.current) return

        let startElement = document.createElement('div')
        startElement.innerHTML = `<h1>A</h1>`
        startElement.className = 'startPointMarker'
        const markerStart = new mapboxgl.Marker(startElement).setLngLat([trip.from.lng || 0, trip.from.lat || 0]).addTo(map.current)
        // if (!trip.destination.lat) return
        let destinationElement = document.createElement('div')
        destinationElement.innerHTML = `<h1>B</h1>`
        destinationElement.className = 'endPointMarker'
        const markerDestination = new mapboxgl.Marker(destinationElement).setLngLat([trip.to.lng || 0, trip.to.lat || 0]).addTo(map.current)

        return () => {
            // console.log('remove')
        }
    }, [map])

    /**
    * Render mapbox marker foe vehicles
    */
    const updateMarkers = (dataVehicle) => {

        const { payload_fields, ...rest } = dataVehicle

        const { lat, lng } = getLatLngFromPayload(payload_fields)
        const newMarkers = markers
        newMarkers[dataVehicle.hardware_serial] = { lng: Number(lng), lat: Number(lat), route: dataVehicle.route, ...rest }
        setMarkers(currentMarkers => ({ ...currentMarkers, ...newMarkers }))
    }

    /**
    * Caculate (avgVelocity, timeToArrive, distanceToArrive) from vehicles data
    */
    useEffect(() => {
        const h_serials = Object.keys(markers)
        // console.log(h_serials);
        const arrayMarkers = []

        const vehiclesRichData = h_serials.map(serial => {

            // Calculate time to arrive to A point
            const nearPointToStart = getNearPointInTrajectory(routeSelected, [trip.from.lat, trip.from.lng])
            const nearPointVehicle = getNearPointInTrajectory(routeSelected, [markers[serial].lat, markers[serial].lng])
            let distanceToArrive = 0

            const fullPath = routeSelected.path.reduce((acc, current, idx) => {
                const distance = acc + (getDistanceFromLatLonInKm([current.lat, current.lng], [routeSelected.path[idx + 1]?.lat, routeSelected.path[idx + 1]?.lng]) || 0)
                return distance
            }, 0)
            if (nearPointToStart > nearPointVehicle) {

                const semiPath = routeSelected.path.slice(nearPointVehicle, nearPointToStart)

                distanceToArrive = semiPath.reduce((acc, current, idx) => {
                    const distance = acc + (getDistanceFromLatLonInKm([current.lat, current.lng], [semiPath[idx + 1]?.lat, semiPath[idx + 1]?.lng]) || 0)
                    return distance
                }, 0)
            } else if (nearPointVehicle > nearPointToStart) {
                const semiPathToLastPointRoute = routeSelected.path.slice(nearPointVehicle, routeSelected.path.length - 1)
                const semiPathFromFirstPointRoute = routeSelected.path.slice(0, nearPointToStart)

                const semiPath = [...semiPathToLastPointRoute, semiPathFromFirstPointRoute]

                distanceToArrive = semiPath.reduce((acc, current, idx) => {
                    const distance = acc + (getDistanceFromLatLonInKm([current.lat, current.lng], [semiPath[idx + 1]?.lat, semiPath[idx + 1]?.lng]) || 0)
                    return distance
                }, 0)
            }

            const sumVelocity = markers[serial].last_5_transit.reduce((acc, current) => acc + Number(current.velocity), 0)
            const avgVelocity = sumVelocity / 5
            const timeToArrive = (distanceToArrive / avgVelocity) * 60
            const timaToArriveFormat = `${moment.duration(timeToArrive, 'minutes').minutes()}`

            let el = document.createElement('div')
            el.innerHTML = `<h1>${markers[serial]?.route?.name}</h1>`
            el.className = 'vehicleMarker'
            // el.style.backgroundColor = `${markers[serial]?.route?.color}`
            const mark = new mapboxgl.Marker(el).setLngLat([markers[serial].lng, markers[serial].lat])
                .setPopup(new mapboxgl.Popup({ offset: 25, className: '' })
                    .setHTML(`<b>Serial: ${serial}</b>
            <p>Velocidad: ${Number.parseFloat(markers[serial]['velocity']).toFixed(2)} Km/hr</p>
            <p>Pasajeros: ${markers[serial]['passengers']}</p>
            <p>Tiempo de llegada: ${timaToArriveFormat} minutos</p>
            <p>Distancia al vehiculo: ${distanceToArrive?.toFixed(2)} Km</p>
            `
                    ))
                .addTo(map.current)
            const { lat: previusLat, lng: previusLng } = getLatLngFromPayload(markers[serial].last_5_transit[1]['payload_fields'])
            const bearing = calculateBearing(previusLat, previusLng, markers[serial].lat, markers[serial].lng)
            el.style.transform = el.style.transform + `rotate(${bearing}deg)`
            arrayMarkers.push(mark)

            return {
                avgVelocity,
                timeToArrive,
                distanceToArrive,
                fullPath,
                ...markers[serial]
            }
        })
        setVehiclesRichData(vehiclesRichData)
        return () => arrayMarkers.map(m => m.remove())
    }, [markers])


    /**
    * Subscribe to every hardware serial event(vehicle)
    */
    useEffect(() => {

        vehicles?.map(vehicle => {
            socket.on(vehicle.hardware_serial, data => {
                console.log('last data',data)
                updateMarkers(data)
            })
        })

        return () => {
            vehicles?.map(vehicle => {
                socket.removeListener(vehicle.hardware_serial, data => { console.log(data) })
                socket.off()
                setMarkers({})
            })
        }
    }, [vehicles])

    /**
    * Get nearby routes to make the trip successfully
    */
    useEffect(() => {
        const probableRoutesFrom = getNearRoutes(routeList, [trip.from.lat, trip.from.lng], 1)
        const probableRoutesTo = getNearRoutes(probableRoutesFrom, [trip.to.lat, trip.to.lng], 1)

        // check trajectory direction
        const routesResult = getNearRoutesTrajectory(probableRoutesTo, [trip.from.lat, trip.from.lng], [trip.to.lat, trip.to.lng])

        setRoutesResult(routesResult)
    }, [routeList, trip])

    /**
    * Render route path on map
    */
    useEffect(() => {

        if (!map.current) return
        const coords = routeSelected.path.map(p => ([p.lng, p.lat]))
        if (!coords.length) return
        if (!map.current.getSource('route')) {
            map.current?.addSource('route', { type: 'geojson', data: initialRoute })
        }
        if (map.current.getSource('route')) {
            map.current.removeLayer('route')
            console.log('source already exist')
            map.current.getSource('route').setData(initialRoute)
            // const

            map.current.addLayer({
                id: 'route',
                type: 'line',
                source: 'route',
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': routeSelected.color || '#03AA46',
                    'line-width': 8,
                    'line-opacity': 0.8
                }
            })
            getVehicles()
            return
        }
    }, [routeSelected, map])

    useEffect(() => {

        if (!map.current) return
        if (!pointsInRoute?.nearFrom?.lat) return

        const markers = Object.keys(pointsInRoute).map(key => createMarker({
            position: [pointsInRoute[key].lng, pointsInRoute[key].lat],
            map: map.current
        }))
        return () => {
            markers.forEach(m => m.remove())
        }
    }, [pointsInRoute])

    /**
    * Get all vehicles realted to a route
    */
    const getVehicles = () => {
        const url = `${process.env.REACT_APP_ENDPOINT}/api/vehicles/route/`;
        fetch(url + routeSelected._id)
            .then((resp) => resp.json())
            .then(vehicles => {
                setVehicles(vehicles.vehicles)
                if (vehicles.vehicles.length === 0) {

                    setOpenWarning({
                        open: true,
                        type: 'warning',
                        message: "No se encontraron vehículos"
                    })
                }

            }).catch((err) => { console.log(err) });
    }

    const handleStartPointChange = () => {
        const probableRoutesFrom = getNearRoutes(routeList, [trip.from.lat, trip.from.lng], 1)
        const probableRoutesTo = getNearRoutes(probableRoutesFrom, [trip.to.lat, trip.to.lng], 1)

        // check trajectory direction
        const routesResult = getNearRoutesTrajectory(probableRoutesTo, [trip.from.lat, trip.from.lng], [trip.to.lat, trip.to.lng])
        setRoutesResult(routesResult)
        if (routesResult.length === 0) {
            setOpenWarning({
                open: true,
                type: 'warning',
                message: "No se encontraron rutas para este viaje"
            })
        }
    }

    // Set route and near point in route
    const handleSelectRoute = (route) => {
        const nearPointToStart = getNearPointInTrajectory(route, [trip.from.lat, trip.from.lng])
        const nearPointToEnd = getNearPointInTrajectory(route, [trip.to.lat, trip.to.lng])

        const coords = route.path.map(({ id, ...rest }) => rest)
        const { center, maxLat, minLat, maxLng, minLng } = getCenterFromPath(coords)
        const zoom = getZoomValue({ maxLat, minLat, maxLng, minLng })
        map.current.flyTo({
            center: [center.lng, center.lat],
            zoom,
        })

        setRouteSelected(route)
        setPointsInRoute({
            nearFrom: route.path[nearPointToStart],
            nearTo: route.path[nearPointToEnd]
        })
    }

    const handleAccordion = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false)
    }

    const handleShowResult = () => {
        setShowResultSection(!showResultSection)
    }

    const handleCloseWarning = () => {
        setOpenWarning(current => ({ ...current, open: false }))
    }



    return (
        // <div className='relative'>
        <div className='absolute top-0 right-0 left-0 bottom-0 md:pt-16' style={{}} >
            <div ref={setRef} className="w-full flex flex-1 h-full rounded-lg" />
            <div className={`transform ease-in-out transition-all duration-300 w-full h-60 bg-white fixed bottom-0 shadow-2xl rounded-t-2xl p-4 md:p-8 ${showResultSection ? 'translate-y-0' : 'translate-y-full'} `}>
                <div onClick={handleShowResult} className={'absolute cursor-pointer right-0 left-0 animate-bounce text-center '} style={{ top: '-90px' }} >
                    <KeyboardArrowUp style={{ width: '35px', height: '35px', opacity: '0.8' }} className={`bg-white rounded-full transform ease-in-out transition-all duration-300 ${showResultSection ? 'rotate-180' : ''} `} />
                </div>
                <div className='absolute right-0 left-0 px-8' style={{ top: '-50px' }} >
                    <button type="button" className={`${t_btn} w-full`} onClick={handleStartPointChange}  >
                        Buscar Rutas
                    </button>
                </div>
                <div className='w-full h-full overflow-y-auto'>
                    {
                        routesResult.length === 0 ?
                            <div>
                                {/* Buscando Rutas disponibles para llegar a tu destino... */}
                            </div> :
                            <div className='flex'>
                                <div className='flex-1 mx-2 divide-x-0' >
                                    <p>Rutas</p>
                                    {
                                        routesResult?.map((r, index) => (
                                            <Slide key={index} direction="up" in={routesResult?.length > 0} mountOnEnter unmountOnExit>
                                                <button onClick={() => handleSelectRoute(r)} className='flex shadow w-full mb-2 items-center focus:outline-none hover:shadow-md'>
                                                    <p className='bg-blue-500 p-2 rounded w-10 h-10 text-center text-white shadow hover:shadow-md font-bold' style={{ backgroundColor: `${r.color}` }} >{r.name}</p>
                                                    <div className='p-2'>
                                                        <p>Tarifa: {r.fare}</p>
                                                    </div>
                                                </button>
                                            </Slide>
                                        ))
                                    }
                                </div>
                                <Divider orientation="vertical" />
                                <div className='flex-1 mx-2 h-full overflow-auto'>
                                    <p>Vehiculos</p>
                                    {
                                        routeSelected && vehicles?.length > 0 && <div className=' h-full w-full'>
                                            {
                                                vehicles?.map((vehicle, index) => {
                                                    return (
                                                        <Accordion key={index} className='card' expanded={expanded == vehicle.placa} onChange={handleAccordion(vehicle.placa)} >
                                                            <AccordionSummary
                                                                expandIcon={<ExpandMore />}
                                                                aria-controls={vehicle.placa}
                                                                id={vehicle.placa}
                                                            >
                                                                <Typography>{vehicle.placa}</Typography>
                                                            </AccordionSummary>
                                                            <AccordionDetails>
                                                                <ul>
                                                                    <Typography>Tiempo de llegada: {`${moment.duration(vehiclesRichData[index]?.timeToArrive, 'minutes').minutes()} minutos`}</Typography>
                                                                    <Typography>Pasajeros: {vehiclesRichData[index]?.passengers}</Typography>
                                                                    <Typography>Velocidad: {vehiclesRichData[index]?.velocity}</Typography>
                                                                    <Typography>Velocidad prom: {vehiclesRichData[index]?.avgVelocity}</Typography>
                                                                </ul>
                                                            </AccordionDetails>
                                                        </Accordion>
                                                    )
                                                })
                                            }
                                        </div>
                                    }
                                </div>
                            </div>
                    }
                </div>
            </div>
            <Snackbar open={openWarning.open} autoHideDuration={5000} onClose={handleCloseWarning}>
                <Alert onClose={handleCloseWarning} severity="warning" >
                    {openWarning.message}
                </Alert>
            </Snackbar>
        </div>
    )
}

export default ResumeTrip
