import React, { useState, useMemo, useEffect, useRef } from 'react'
import { connect, useDispatch } from 'react-redux'
import { Link, useHistory, useParams } from 'react-router-dom';
import { t_btn } from '../../constants/tailwindStyles';
import * as Actions from '../../store/actions'
import { Snackbar, TextField } from '@material-ui/core';
import { CirclePicker } from 'react-color';

import mapboxgl from 'mapbox-gl'
import { ArrowBack } from '@material-ui/icons';
import axios from 'axios';
import Alert from '../../components/Alert';
import { useDebouncedFn } from 'beautiful-react-hooks';
import BackNavigation from '../../components/BackNavigation';

mapboxgl.accessToken = 'pk.eyJ1IjoicmFmYWJlbGFsY2F6YXIiLCJhIjoiY2tteGo0cmk3MDZxZjJ2bXUwMHRyOWhmOSJ9.54HQH5wF2poXqTlRcTpRAA';

const initialPoint = {
    lng: -76.613,
    lat: 2.44923,
    zoom: 12
}

/**
* Render UI for edit route params
*
*/
const RouteEdit = ({ main, ...props }) => {
    const { selectedRoute: route, user } = main

    let { id } = useParams();

    const [openAlert, setOpenAlert] = useState(false);
    const [error, setError] = useState();

    const mapDiv = useRef(null)
    const myMap = useRef(null)

    const dispatch = useDispatch()
    const history = useHistory()

    /**
    * Object configuration to draw a path on map
    *
    */
    const initialRoute = useMemo(() => ({
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': route?.path?.map(item => ([item.lng, item.lat]))
                }
            }
        ]
    }), [route])

    /**
    * Render mapbox map
    *
    */
    useEffect(() => {
        const map = new mapboxgl.Map({
            container: mapDiv.current,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [initialPoint.lng, initialPoint.lat],
            zoom: initialPoint.zoom
        })
        myMap.current = map
        return () => {
        }
    }, [])

    /**
    * Render path route on map
    *
    */
    useEffect(() => {
        myMap.current?.on('load', () => {
            if (myMap.current.getSource('route')) {
                console.log('source already exist')
                return
            }

            myMap.current?.addSource('route', { type: 'geojson', data: initialRoute })

            myMap.current?.addLayer({
                id: 'route',
                type: 'line',
                source: 'route',
                paint: {
                    // 'fill-color': "#000000",
                    'line-color': route.color,
                    'line-width': 10,
                    'line-opacity': 0.75,
                    // color: "#000000"
                }
            })
        })
    }, [])

    /**
    * Change route color on map
    *
    */
    useEffect(() => {
        if (!myMap.current) return
        if (myMap.current.getSource('route')) {
            console.log('source already exist')
            myMap.current.setPaintProperty('route', 'line-color', route.color)
            return
        }
    }, [route.color])

    const handleRouteSelect = (e) => {
        // console.log(e.target.name, e.target.value)
        // dispatch to Redux store
        dispatch(Actions.setSelectedRoute({
            [e.target.name]: e.target.value
        }))
    }

    const handleChangeColor = (color, e) => {
        dispatch(Actions.setSelectedRoute({
            color: color.hex
        }))
    }

    const navigateRoutes = useDebouncedFn(()=>{
        history.push('/routes')
    },1000)

    const editeRouteRequest = (e) => {
        axios.put(`${process.env.REACT_APP_ENDPOINT}/api/routes/${route._id}`, {
            name: route.name,
            fare: Number(route.fare),
            path: route.path,
            color: route.color,
            status: route.status === 'Activa' ? true : false,
            org_uid: user.uid
        }).then(res => {
            setOpenAlert(true)
            navigateRoutes()
        }).catch(err => {
            setOpenAlert(true)
            setError(err)
            console.log(err)
        })
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenAlert(false);
    }

    return (
        <div>
            <BackNavigation to='/routes' text='Editar ruta'/>
            <form className='h-screen py-16 md:py-24 '>
                <div className="-mx-3 md:flex mb-6">
                    <div className="md:w-1/2 px-3 mb-6 md:mb-0">
                        <TextField
                            className="w-full"
                            label="Número de Ruta"
                            name="name"
                            variant="outlined"
                            onChange={handleRouteSelect}
                            value={route.name}
                        />
                    </div>
                    <div className="md:w-1/2 px-3 mb-6 md:mb-0">
                        <TextField
                            className="w-full"
                            label="Tarifa"
                            name="fare"
                            variant="outlined"
                            onChange={handleRouteSelect}
                            value={route.fare}
                        />
                    </div>
                </div>
                <div className="w-full mb-8">
                    <p className='p-1 text-gray-500' >Select a color</p>
                    <CirclePicker
                        width='100%'
                        color={route.color}
                        onChange={handleChangeColor}
                    />
                </div>
                <div className='relative w-full h-4/6 flex-col rounded ' >
                    <Link to={`/mapEdit/${id}`} className='' >
                        <div className="z-10 absolute flex w-full inset-0 justify-center justify-items-center hover:bg-white hover:bg-opacity-50 pointer">
                            <p className='self-center ' >Click para trazar ruta</p>
                        </div>
                    </Link>
                    <div ref={mapDiv} className="w-full flex flex-1 h-full rounded-lg"  >
                    </div>
                    <div className="md:w-full flex justify-center my-4">
                        <button type="button" onClick={editeRouteRequest} className={`${t_btn} w-full`}>
                            Editar ruta
                        </button>
                    </div>
                    <div className='h-8' ></div>
                </div>
            </form>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity={error? 'error': "success"}>
                    {
                        error ?
                        'Ha ocurrido un error al editar la ruta':
                        'Se ha editado correctamente'
                    }
                </Alert>
            </Snackbar>
        </div>
    )
}
const mapStateToProps = state => {
    return {
        main: state.main
    }
}

export default connect(mapStateToProps, null)(RouteEdit)
