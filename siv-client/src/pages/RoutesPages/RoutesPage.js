import React, { useEffect, useMemo, useRef, useState } from 'react'

import mapboxgl from 'mapbox-gl'
import { v4 as uuid } from 'uuid';
import { connect, useDispatch } from 'react-redux';
import * as Actions from '../../store/actions'
import { useHistory } from 'react-router';

mapboxgl.accessToken = 'pk.eyJ1IjoicmFmYWJlbGFsY2F6YXIiLCJhIjoiY2tteGo0cmk3MDZxZjJ2bXUwMHRyOWhmOSJ9.54HQH5wF2poXqTlRcTpRAA';

const initialPoint = {
    lng: -76.613,
    lat: 2.44923,
    zoom: 13
}

/**
* Render UI for create route path in map
*
*/
const RoutesPage = ({ main }) => {

    const { route: route_tmp, user } = main

    const mapDiv = useRef(null)
    const myMap = useRef(null)
    const [coords, setCoords] = useState(initialPoint)
    const history = useHistory()

    const [route, setRoute] = useState(route_tmp.path)

    const dispatch = useDispatch()

    const initialRoute = useMemo(() => ({
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': route.map(item => ([item.lng, item.lat]))
                }
            }
        ]
    }), [route])

    /**
    * Render mapbox map
    *
    */
    useEffect(() => {

        var map = new mapboxgl.Map({
            container: mapDiv.current,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [coords.lng, coords.lat],
            zoom: coords.zoom
        });

        myMap.current = map

    }, [])

    /**
    * Render path route on map
    *
    */
    useEffect(() => {
        myMap.current?.on('load', function () {
            if (myMap.current.getSource('route')) {
                console.log('source already exist')
                return
            }
            myMap.current?.addSource('route', { type: 'geojson', data: initialRoute })
            console.log('load')

            myMap.current?.addLayer({
                id: 'route',
                type: 'line',
                source: 'route',
                paint: {
                    // 'fill-color': "#000000",
                    'line-color': "#ffaa00",
                    'line-width': 10,
                    'line-opacity': 0.75,
                    // color: "#000000"
                }
            })

            route.map(item => {
                var marker = new mapboxgl.Marker({
                    draggable: true
                })
                    .setLngLat([item.lng, item.lat])
                    .addTo(myMap.current)
                    // .setPopup(popup)

            })

        })

        return () => {
            if (myMap.current) {
                myMap.current.off('load')
            }
        }

    }, [])

    /**
    * Render route markers on map
    *
    */
    useEffect(() => {

        myMap.current?.on('click', (e) => {

            var marker = new mapboxgl.Marker({
                draggable: true
            })
                .setLngLat([e.lngLat.lng, e.lngLat.lat])
                .addTo(myMap.current)

            marker.id = uuid()

            setRoute(route => [...route, {
                id: marker.id,
                lng: e.lngLat.lng,
                lat: e.lngLat.lat
            }])

            marker.on('dragend', ({ target }) => {
                const { id } = target;
                const { lng, lat } = target.getLngLat();

                setRoute(route => {
                    const idx = route.findIndex(item => item.id === id)
                    const newRoute = [...route]
                    newRoute[idx] = { ...newRoute[idx], lng, lat }

                    return newRoute
                })
            })

        })

        return () => {
            if (myMap.current) {
                myMap.current.off('move')
            }
        }
    }, [])



    useEffect(() => {
        if (!myMap.current.getSource('route')) return
        myMap.current.getSource('route').setData({
            "type": "FeatureCollection",
            "features": [{
                "type": "Feature",
                // "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates": route.map(item => ([item.lng, item.lat]))

                }
            }]
        })
        // myMap.current?.getSource('route').setData(initialRoute)
    }, [initialRoute])


    const handleSavePath = () => {
        dispatch(Actions.setRoutePath(route))
        history.push('/createRoute')

    }

    return (
        <>
            <div className='fixed z-10 left-2 bg-white bg-opacity-90 rounded p-1 md:p-4' >
                <p>Latitude: {coords.lat}</p>
                <p>Longitude: {coords.lng} </p>
                <p>Zoom: {coords.zoom} </p>
            </div>
            <div ref={mapDiv} className="absolute top-0 right-0 left-0 bottom-0">


            </div>
            <div className='fixed z-10 right-8 bottom-8  bg-opacity'>
                <button
                    onClick={handleSavePath}
                    className="md:block text-white px-4 w-auto h-10 bg-blue-500 rounded-full hover:bg-blue-700 active:shadow-lg mouse shadow transition ease-in duration-200 focus:outline-none">
                    <svg viewBox="0 0 20 20" enableBackground="new 0 0 20 20" className="w-6 h-6 inline-block">
                        <path fill="#FFFFFF" d="M16,10c0,0.553-0.048,1-0.601,1H11v4.399C11,15.951,10.553,16,10,16c-0.553,0-1-0.049-1-0.601V11H4.601
                                    C4.049,11,4,10.553,4,10c0-0.553,0.049-1,0.601-1H9V4.601C9,4.048,9.447,4,10,4c0.553,0,1,0.048,1,0.601V9h4.399
                                    C15.952,9,16,9.447,16,10z" />
                    </svg>
                    <span>Guardar trayectoria</span>
                </button>
            </div>
        </>
    )
}

const mapStateToProps = state => {
    return {
        main: state.main
    }
}

export default connect(mapStateToProps, null)(RoutesPage)
