import React, { useMemo, useEffect, useRef } from 'react'
import { connect, useDispatch } from 'react-redux'
import { Link, useHistory } from 'react-router-dom';
import { t_btn } from '../../constants/tailwindStyles';
import * as Actions from '../../store/actions';


import mapboxgl from 'mapbox-gl'
import { ArrowBack } from '@material-ui/icons';
mapboxgl.accessToken = 'pk.eyJ1IjoicmFmYWJlbGFsY2F6YXIiLCJhIjoiY2tteGo0cmk3MDZxZjJ2bXUwMHRyOWhmOSJ9.54HQH5wF2poXqTlRcTpRAA';

const initialPoint = {
    lng: -76.613,
    lat: 2.44923,
    zoom: 13
}

/**
* Render UI for show a route path on map
*
*/
const RouteEdit = ({ main }) => {
    const { selectedRoute: route, user } = main

    const mapDiv = useRef(null)
    const myMap = useRef(null)
    const dispatch = useDispatch()
    const history = useHistory()


    const initialRoute = useMemo(() => ({
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': route?.path?.map(item => ([item.lng, item.lat]))
                }
            }
        ]
    }), [route])

    useEffect(() => {

        const map = new mapboxgl.Map({
            container: mapDiv.current,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [initialPoint.lng, initialPoint.lat],
            zoom: initialPoint.zoom
        })

        myMap.current = map

        return () => {

        }
    }, [])

    useEffect(() => {
        myMap.current?.on('load', () => {
            if (myMap.current.getSource('route')) {
                console.log('source already exist')
                return
            }

            myMap.current?.addSource('route', { type: 'geojson', data: initialRoute })

            myMap.current?.addLayer({
                id: 'route',
                type: 'line',
                source: 'route',
                properties:{ 
                    description:'Ruta 1'
                },
                paint: {
                    // 'fill-color': "#000000",
                    'line-color': route.color || "#ffaa00",
                    'line-width': 10,
                    'line-opacity': 0.6,
                    // 'color': "#000000"
                }
            })
        })

    })

    const regresar = () => {
        history.push('/routes')
    }
    return (
        <div>
            <div className='fixed z-10 right-2 bg-white bg-opacity-90 rounded p-1 md:p-4' >
                <p>Número de Ruta: {route.name}</p>
                <p>Tarifa: {route.fare} </p>
            </div>
            <div className='absolute top-0 right-0 left-0 bottom-0 ' >
                <div ref={mapDiv} className="w-full flex flex-1 h-full rounded-lg"  >
             </div>
        </div>
        <div className='fixed z-10 left-2 bg-white bg-opacity-90 rounded p-1 md:p-4' onClick={regresar}>
            <ArrowBack>

            </ArrowBack>
        </div>
        
        </div>
    )
}
const mapStateToProps = state => {
    return {
        main: state.main
    }
}

export default connect(mapStateToProps, null)(RouteEdit)