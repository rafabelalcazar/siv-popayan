import React, { useEffect, useMemo, useRef, useState } from 'react'

import mapboxgl from 'mapbox-gl'
import { v4 as uuid } from 'uuid';
import { connect, useDispatch } from 'react-redux';
import * as Actions from '../../store/actions'
import { useHistory, useParams } from 'react-router';
import { ArrowBack } from '@material-ui/icons';
import axios from 'axios';
import { t_btn } from '../../constants/tailwindStyles';
import FixedButton from '../../components/FixedButton';

// TODO: Change API KEY
mapboxgl.accessToken = 'pk.eyJ1IjoicmFmYWJlbGFsY2F6YXIiLCJhIjoiY2tteGo0cmk3MDZxZjJ2bXUwMHRyOWhmOSJ9.54HQH5wF2poXqTlRcTpRAA';

const initialPoint = {
    lng: -76.613,
    lat: 2.44923,
    zoom: 13
}

const initialState = {
    path: [{
        id: "9160ca74-3ef6-4bbf-aafe-097d78f1d720",
        lat: 2.4487463273663224,
        lng: -76.62397528001165
    }],

}

/**
* Render UI for edit route path
*
*/
const RoutesPage = ({ main }) => {
    const { selectedRoute, user } = main
    let { id } = useParams();
    const mapDiv = useRef(null)
    const myMap = useRef(null)
    const [coords, setCoords] = useState(initialPoint)
    const history = useHistory()
    const [route, setRoute] = useState(initialState)
    const dispatch = useDispatch()


    // console.log(route)
    useEffect(() => {
        // Actions.getRoute(id)
        dispatch(Actions.getRoute(id))

        // const url = `https://siv-backend-popayan.herokuapp.com/api/routes/${id}`;
        // debugger
        // axios.get(url)
        // .then(res=> setRoute(res.data.route))
        // // fetch(url).then(res => res.json()).then(data => setRoute(data.route))
        // console.log('fetch data')

        return () => { }
    }, [])

    useEffect(() => {
        setRoute(selectedRoute)
        return () => {

        }
    }, [selectedRoute])

    const initialRoute = useMemo(() => {

        return ({
            'type': 'FeatureCollection',
            'features': [
                {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': selectedRoute?.path?.map(item => ([item.lng, item.lat]))
                    }
                }
            ]
        })
    }, [selectedRoute])

    useEffect(() => {

        var map = new mapboxgl.Map({
            container: mapDiv.current,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [coords.lng, coords.lat],
            zoom: coords.zoom
        });

        myMap.current = map
    }, [])



    useEffect(() => {
        myMap.current?.on('load', function () {
            if (myMap.current.getSource('route')) {
                console.log('source already exist')
                return
            }
            myMap.current?.addSource('route', { type: 'geojson', data: initialRoute })
            console.log('load')

            myMap.current?.addLayer({
                id: 'route',
                type: 'line',
                source: 'route',
                paint: {
                    // 'fill-color': "#000000",
                    'line-color': selectedRoute.color,
                    'line-width': 10,
                    'line-opacity': 0.75,
                    // color: "#000000"
                }
            })

            const markers = selectedRoute?.path?.map(mark => {

                console.log(mark)

                let marker = new mapboxgl.Marker({
                    draggable: true
                })
                    .setLngLat([mark.lng, mark.lat])
                    .addTo(myMap.current)
                marker.id = mark.id

                return marker
            })

            markers.map(m => {

                m.on('dragend', ({ target }) => {
                    const { id } = target;
                    const { lng, lat } = target.getLngLat();
                    console.log(id, lng, lat)

                    dispatch(Actions.setPathToEdit({ id, lng, lat }))
                })
            })




        })

        return () => {
            if (myMap.current) {
                myMap.current.off('load')
            }
        }

    }, [])

    useEffect(() => {
        myMap.current?.on('move', () => {
            const { lat, lng } = myMap.current.getCenter();
            const zoom = myMap.current.getZoom();
            setCoords({
                lat,
                lng,
                zoom
            })
            // console.log('movimg map')
        })

        myMap.current?.on('click', (e) => {

            // const { lngLat } = e.lngLat
            // console.log(e.lngLat.lng, e.lngLat.lat)
            let marker = new mapboxgl.Marker({
                draggable: true
            })
                .setLngLat([e.lngLat.lng, e.lngLat.lat])
                .addTo(myMap.current)

            marker.id = uuid()

            dispatch(Actions.addMarkerToPath({
                id: marker.id,
                lng: e.lngLat.lng,
                lat: e.lngLat.lat
            }))

            marker.on('dragend', ({ target }) => {
                const { id } = target;
                const { lng, lat } = target.getLngLat();
                console.log(lng, lat)

                dispatch(Actions.setPathToEdit({ id, lng, lat }))
            })

        })

        return () => {
            if (myMap.current) {
                myMap.current.off('move')
            }
        }
    }, [])


    // Mostrar trazo de la ruta
    useEffect(() => {
        if (!myMap.current.getSource('route')) return
        myMap.current.getSource('route').setData({
            "type": "FeatureCollection",
            "features": [{
                "type": "Feature",
                // "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates": selectedRoute?.path?.map(item => ([item.lng, item.lat]))

                }
            }]
        })
    }, [initialRoute])


    const handleSavePath = () => {
        dispatch(Actions.setRoutePath(selectedRoute))
        // console.log(selectedRoute,`${process.env.REACT_APP_ENDPOINT}/api/routes/${id}`)
        axios.put(`${process.env.REACT_APP_ENDPOINT}/api/routes/${id}`,{
            name: selectedRoute.name,
            fare: selectedRoute.fare,
            path: selectedRoute.path,
            status: selectedRoute.status
        }).then(res=>console.log(res) )
        .catch(err=>console.log('why',err))
        history.push('/routes')

    }

    const regresar = () => {
        history.push('/routes')
    }
    return (
        <div>
            <div className='fixed z-10 right-2 bg-white bg-opacity-90 rounded p-1 md:p-4' >
                <p>Número de Ruta: {selectedRoute.name}</p>
                <p>Tarifa: {selectedRoute.fare} </p>
            </div>
            <div className='absolute top-0 right-0 left-0 bottom-0 ' >
                <div ref={mapDiv} className="w-full flex flex-1 h-full rounded-lg"  >
                </div>
            </div>
            <div className='fixed z-10 left-2 bg-white bg-opacity-90 rounded p-1 md:p-4' onClick={regresar}>
                <ArrowBack />
            </div>
            <FixedButton title="Editar trayectoria" onClick={handleSavePath} />

        </div>
    )
}

const mapStateToProps = state => {
    return {
        main: state.main
    }
}

export default connect(mapStateToProps, null)(RoutesPage)
