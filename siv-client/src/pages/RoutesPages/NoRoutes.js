import React from 'react'
import { Link } from 'react-router-dom'
import { t_btn } from '../../constants/tailwindStyles'
const NoRoutes = () => {
    return (
        <div className="pt-20 text-gray-800 px-4 md:px-32">
            <p className="my-4">Aun no hay rutas registradas</p>
            <Link to="/createRoute">
                <button className={t_btn} >Crear ruta</button>
            </Link>
        </div>
    )
}

export default NoRoutes
