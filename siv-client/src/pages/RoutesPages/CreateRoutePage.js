import React, { useEffect, useState, useRef, useMemo } from 'react'
import mapboxgl from 'mapbox-gl'
import { Link, useHistory } from 'react-router-dom';
import axios from 'axios';
import { Snackbar, TextField } from '@material-ui/core';
import Alert from '../../components/Alert';
import { connect, useDispatch } from 'react-redux';
import * as Actions from '../../store/actions';
import { t_btn } from '../../constants/tailwindStyles';
import { CirclePicker } from 'react-color';
import BackNavigation from '../../components/BackNavigation';

mapboxgl.accessToken = 'pk.eyJ1IjoicmFmYWJlbGFsY2F6YXIiLCJhIjoiY2tteGo0cmk3MDZxZjJ2bXUwMHRyOWhmOSJ9.54HQH5wF2poXqTlRcTpRAA';

const initialPoint = {
  lng: -76.613,
  lat: 2.44923,
  zoom: 13
}

/**
* Render UI for create a route
*
*/
const CreateRoutePage = ({ main }) => {
  const { route, user } = main

  const [openSnack, setOpenSnack] = useState({
    open: false,
    type: 'error',
    message: 'hey'

  })

  const dispatch = useDispatch()
  const mapDiv = useRef(null)
  const myMap = useRef(null)

  const history = useHistory()

  const initialRoute = useMemo(() => ({
    'type': 'FeatureCollection',
    'features': [
      {
        'type': 'Feature',
        'properties': {},
        'geometry': {
          'type': 'LineString',
          'coordinates': route.path.map(item => ([item?.lng, item?.lat]))
        }
      }
    ]
  }), [route])

  /**
  * Render map 
  *
  */
  useEffect(() => {

    const map = new mapboxgl.Map({
      container: mapDiv.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [initialPoint.lng, initialPoint.lat],
      zoom: initialPoint.zoom
    })

    myMap.current = map

    return () => {

    }
  }, [])

  /**
  * Render a route in map if exists 
  *
  */
  useEffect(() => {
    myMap.current?.on('load', () => {
      if (myMap.current.getSource('route')) {
        return
      }

      myMap.current?.addSource('route', { type: 'geojson', data: initialRoute })

      myMap.current?.addLayer({
        id: 'route',
        type: 'line',
        source: 'route',
        paint: {
          // 'fill-color': "#000000",
          'line-color': "#ffaa00",
          'line-width': 10,
          'line-opacity': 0.75,
          // color: "#000000"
        }
      })

    })

  })


  const createRouteRequest = async () => {

    const {data} = await axios.get(`${process.env.REACT_APP_ENDPOINT}/api/routes`)
    const findRoute = data.routes.find(r => r.name === route.name)

    if(findRoute){
      setOpenSnack({
        open: true,
        type: "warning",
        message: "Ya existe una ruta con este númerp"
      })
      return
    }

    if (!route.name || !route.fare ) {
      setOpenSnack({
        open: true,
        type: "warning",
        message: "La ruta debe tener un nombre y tarifa"
      })
      return
    }

    if (route.path.length <= 1) {
      setOpenSnack({
        open: true,
        type: "warning",
        message: "La trayectoría debe la ruta debe ser de más de un punto"
      })
      return
    }
    
    axios.post(`${process.env.REACT_APP_ENDPOINT}/api/routes/new`, {
      name: route.name,
      fare: route.fare,
      path: [...route.path],
      status: true,
      color: route.color,
      org_uid: user.uid
    }).then(resp => {
      setOpenSnack({
        open: true,
        type: "success",
        message: "Ruta creada correctamente"
      })
      dispatch(Actions.setSelectedRoute({
        path: []
      }))
      dispatch(Actions.setDataRoute({
        name: "",
        fare: '',
        path: []
      }))
      history.push('/routes')

    })
      .catch(err => {
        setOpenSnack({
          open: true,
          type: "error",
          message: "Error al crear ruta"
        })
      })
  }

  const handleRoute = (e) => {
    // dispatch to Redux store
    dispatch(Actions.setDataRoute({
      [e.target.name]: e.target.value
    }))
  }

  const handleChangeColor = (color, e) => {
    dispatch(Actions.setDataRoute({
      color: color.hex
    }))
  }

  const handleCloseSnack = (event, reason) => {
    setOpenSnack(snack => ({ ...snack, open: false }))
  }

  return (
    <div>
      <div>
        <BackNavigation to='/routes' text='Crear ruta' />
        <form className='h-screen py-16 md:py-24'>
          <div className="-mx-3 md:flex mb-6">
            <div className="md:w-1/2 px-3 mb-6 md:mb-0">
              <TextField
                className="w-full"
                label="Número de Ruta"
                name="name"
                variant="outlined"
                onChange={handleRoute}
                value={route.name}
              />
            </div>
            <div className="md:w-1/2 px-3 mb-6 md:mb-0">
              <TextField
                className="w-full"
                label="Tarifa"
                name="fare"
                variant="outlined"
                onChange={handleRoute}
                value={route.fare}
              />
            </div>
          </div>
          <div className="pb-10">
            <p className='p-1 text-gray-500' >Select a color</p>
            <CirclePicker
              width='100%'
              color={route.color}
              onChange={handleChangeColor}
            />
          </div>
          <div className='relative w-full h-4/6 flex-col rounded ' >
            <Link to='/routePath' className='' >
              <div className="z-10 absolute flex w-full inset-0 justify-center justify-items-center hover:bg-white hover:bg-opacity-50 pointer">
                <p className='self-center ' >Click para trazar ruta</p>
              </div>
            </Link>
            <div ref={mapDiv} className="w-full flex flex-1 h-full rounded-lg"  >
            </div>
            <div className="md:w-full flex justify-center my-4">
              <button type="button" onClick={createRouteRequest} className={`${t_btn} w-full`}>
                Crear ruta
              </button>
            </div>
          </div>
          <div className='h-16' ></div>
        </form>
      </div>
      <Snackbar open={openSnack.open} autoHideDuration={6000} onClose={handleCloseSnack}>
        <Alert onClose={handleCloseSnack} severity={openSnack.type}>
          {openSnack.message}
        </Alert>
      </Snackbar>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    main: state.main
  }
}

export default connect(mapStateToProps, null)(CreateRoutePage)