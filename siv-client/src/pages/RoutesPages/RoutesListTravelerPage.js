import React, { useEffect, useState } from 'react'
import {  Map } from '@material-ui/icons'
import Table from '../../components/Table'
import NoRoutes from './NoRoutes'
import { useHistory } from 'react-router-dom'
import { connect, useDispatch } from 'react-redux'
import * as Actions from '../../store/actions'
import axios from 'axios'


/**
* Render a list of all routes registered  
*
*/
const RoutesListTravelerPage = ({ main }) => {

    const [routeList, setRouteList] = useState([])

    const url = `${process.env.REACT_APP_ENDPOINT}/api/routes`;
    const dispatch = useDispatch()
    const history = useHistory()

    useEffect(() => {

        axios.get(url).then(({ data }) => {
            setRouteList(data.routes)
        }).catch(err => {
            console.log(err)
        })
        return () => {
            console.log('Bye List routes')
        }
    }, [])

    // console.log(routeList)

    const data = routeList.map(route => ({
        ...route,
        status: route.status ? 'Activa' : 'Inactiva'
    }))



    const columns = [
        {
            title: 'Ruta',
            field: 'name',
            cellStyle: { width: '10px' },
            render: rowData => {
                return <p className='bg-blue-500 p-2 rounded w-10 h-10 text-center text-white shadow hover:shadow-md font-bold' style={{ backgroundColor: rowData.color }} >{rowData.name}</p>
            }
        },
        {
            title: 'Estado',
            field: 'status'
        },
        {
            title: 'Tarifa',
            field: 'fare',
            render: rowData => {
                return <p className='' > {new Intl.NumberFormat('de-DE').format(rowData.fare)}</p>
            }
        }
    ]


    return (
        <div>
            {/* <h3>Rutas de empresa</h3> */}

            <div className="my-4">
                {
                    data.length === 0 ? <NoRoutes /> :
                        <Table
                            title='Lista de Rutas'
                            columns={columns}
                            data={data}
                            options={{
                                // padding: 'dense',
                                actionsColumnIndex: -1,
                                // header: false,
                                paging: false
                            }
                            }
                            actions={[
                                {
                                    icon: () => <Map className="text-blue-500 border rounded border-blue-500 mx-2" />,
                                    tooltip: 'Ver en mapa',
                                    onClick: (event, rowData) => {
                                        console.log(event)
                                        dispatch(Actions.setSelectedRoute(rowData))
                                        history.push('/verMapa')
                                    }
                                }
                            ]}
                            localization={{
                                header: {
                                    actions: 'Acciones'
                                }
                            }}
                        />

                }
                
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        main: state.main
    }
}

export default connect(mapStateToProps, null)(RoutesListTravelerPage)



