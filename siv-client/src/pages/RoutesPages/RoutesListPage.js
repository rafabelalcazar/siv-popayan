import React, { forwardRef, useEffect, useState } from 'react'
import { Edit, Map } from '@material-ui/icons'
import Table from '../../components/Table'
import NoRoutes from './NoRoutes'
import { Link, useHistory } from 'react-router-dom'
import { connect, useDispatch } from 'react-redux'
import * as Actions from '../../store/actions'
import axios from 'axios'


/**
* Render a list of all routes registered by an enterprise 
*
*/
const RoutesListPage = ({ main }) => {
    const { user } = main

    const [routeList, setRouteList] = useState([])

    const url = `${process.env.REACT_APP_ENDPOINT}/api/routes/`;
    const dispatch = useDispatch()
    const history = useHistory()

    useEffect(() => {

        axios.get(url, { org_uid: user.uid }).then(({ data }) => {
            setRouteList(data.routes)
        }).catch(err => {
            console.log(err)
        })
        return () => {
            console.log('Bye List routes')
        }
    }, [])

    const data = routeList.map(route => ({
        ...route,
        status: route.status ? 'Activa' : 'Inactiva'
    }))



    const columns = [
        {
            title: 'Ruta',
            field: 'name',
            cellStyle: { width: '10px' },
            render: rowData => {
                return <p className='bg-blue-500 p-2 rounded w-10 h-10 text-center text-white shadow hover:shadow-md font-bold' style={{ backgroundColor: rowData.color }} >{rowData.name}</p>
            }
        },
        {
            title: 'Estado',
            field: 'status'
        },
        {
            title: 'Tarifa',
            field: 'fare',
            render: rowData => {
                return <p className='' > {new Intl.NumberFormat('de-DE').format(rowData.fare)}</p>
            }
        }
    ]


    return (
        <div>
            {/* <h3>Rutas de empresa</h3> */}

            <div className="my-4">
                {
                    data.length === 0 ? <NoRoutes /> :
                        <Table
                            title='Lista de Rutas'
                            columns={columns}
                            data={data}
                            options={{
                                // padding: 'dense',
                                actionsColumnIndex: -1,
                                // header: false,
                                paging: false
                            }
                            }
                            actions={[
                                {
                                    icon: () => <Edit className="text-yellow-500 border rounded border-yellow-500 mx-2" />,
                                    tooltip: 'editar',
                                    onClick: (event, rowData) => {
                                        console.log(rowData)
                                        dispatch(Actions.setSelectedRoute(rowData))
                                        history.push(`/routeEdit/${rowData._id}`)
                                    }
                                },
                                {
                                    icon: () => <Map className="text-blue-500 border rounded border-blue-500 mx-2" />,
                                    tooltip: 'Ver en mapa',
                                    onClick: (event, rowData) => {
                                        console.log(event)
                                        dispatch(Actions.setSelectedRoute(rowData))
                                        history.push('/verMapa')
                                    }
                                }
                            ]}
                            localization={{
                                header: {
                                    actions: 'Acciones'
                                }
                            }}
                        />

                }
                <div className="fixed bottom-8 right-8 md:hidden">
                    <Link to="/createRoute">
                        <button
                            className="p-0 w-12 h-12 bg-blue-400 rounded-full hover:bg-blue-700 active:shadow-lg mouse shadow transition ease-in duration-200 focus:outline-none">
                            <svg viewBox="0 0 20 20" enableBackground="new 0 0 20 20" className="w-6 h-6 inline-block">
                                <path fill="#FFFFFF" d="M16,10c0,0.553-0.048,1-0.601,1H11v4.399C11,15.951,10.553,16,10,16c-0.553,0-1-0.049-1-0.601V11H4.601
                                    C4.049,11,4,10.553,4,10c0-0.553,0.049-1,0.601-1H9V4.601C9,4.048,9.447,4,10,4c0.553,0,1,0.048,1,0.601V9h4.399
                                    C15.952,9,16,9.447,16,10z" />
                            </svg>
                        </button>
                    </Link>
                </div>
                <div className="fixed bottom-8 right-8">
                    <Link to="/createRoute">
                        <button
                            className="hidden md:block text-white px-4 w-auto h-10 bg-blue-400 rounded-full hover:bg-blue-700 active:shadow-lg mouse shadow transition ease-in duration-200 focus:outline-none">
                            <svg viewBox="0 0 20 20" enableBackground="new 0 0 20 20" className="w-6 h-6 inline-block">
                                <path fill="#FFFFFF" d="M16,10c0,0.553-0.048,1-0.601,1H11v4.399C11,15.951,10.553,16,10,16c-0.553,0-1-0.049-1-0.601V11H4.601
                                    C4.049,11,4,10.553,4,10c0-0.553,0.049-1,0.601-1H9V4.601C9,4.048,9.447,4,10,4c0.553,0,1,0.048,1,0.601V9h4.399
                                    C15.952,9,16,9.447,16,10z" />
                            </svg>
                            <span className='mx-24' >Crear ruta</span>
                        </button>
                    </Link>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        main: state.main
    }
}

export default connect(mapStateToProps, null)(RoutesListPage)



