import React,{useContext, useEffect} from 'react'
import { SocketContext } from '../context/SocketContext'

const Test = () => {
    
    const { socket } = useContext(SocketContext)

    useEffect(()=>{
        socket.on('0056A03AAAE4F07C',data=>{
            console.log(data)
        })

    },[])
    return (
        <div>
            Hola test
        </div>
    )
}

export default Test
