import React, { useState } from 'react'
import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import { AppBar, IconButton, Toolbar, Button } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import { DateTimePicker } from '@material-ui/pickers';
import Table from '../../../components/Table';
import axios from 'axios';

const ReportDetail = ({ open, onClose, hardware_serial, initialDate, endDate }) => {

    // const [initialDate, setInitialDate] = useState()
    // const [endDate, setEndDate] = useState()
    const [transitData, setTransitData] = useState()

    const handleGenerateReport = () => {

        const getTransitVehicle = async () => {

            const url = `${process.env.REACT_APP_ENDPOINT}/api/reports/${hardware_serial}`
            console.log(hardware_serial)
            const { data } = await axios.post(url, {
                initialDate,
                endDate

            })
            return data
        }

        getTransitVehicle().then(resp => {
            const { transits } = resp
            setTransitData(transits)
        })

    }

    const columns = [
        {
            title: 'Serial',
            field: 'hardware_serial'
        },
        {
            title: "Confirmado",
            field: "confirmed",
        },
        {
            title: "Velocidad",
            field: 'velocity'
        },
        {
            title: "Pasajeros",
            field: 'passengers'
        },
        {
            title: 'Reintento',
            field: 'is_retry'
        },
        {
            title: 'Fecha',
            field: 'creation_date'
        }
    ]


    return (
        <>
            <Dialog fullScreen onClose={onClose} aria-labelledby="simple-dialog-title" open={open}>
                <div className="h-full w-full">
                    <AppBar>
                        <Toolbar>
                            <IconButton edge="start" color="inherit" onClick={onClose}>
                                <Close />
                            </IconButton>
                            <Typography variant="h6">Reporte detallado</Typography>
                        </Toolbar>
                    </AppBar>
                    <div className=" mt-24 m-4 md:mx-16">
                        <Typography>Generar reporte del vehiculo {hardware_serial}</Typography>
                        <div className="flex justify-between">

                            <DateTimePicker
                                className="w-1/3"
                                autoOk={true}
                                margin="normal"
                                id="date-picker-dialog"
                                label="Desde"
                                inputVariant="outlined"
                                variant="inline"
                                format="dd/MM/yyyy hh:mm a"
                                value={initialDate}
                                disabled
                                // onChange={(e) => setInitialDate(e)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                            <DateTimePicker
                                className="w-1/3"
                                autoOk={true}
                                margin="normal"
                                id="date-picker-dialog"
                                label="Hasta"
                                inputVariant="outlined"
                                variant="inline"
                                format="dd/MM/yyyy hh:mm a"
                                value={endDate}
                                disabled
                                // onChange={(e) => setEndDate(e)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                            <Button variant="contained" onClick={handleGenerateReport}>Generar reporte</Button>
                        </div>
                    </div>
                    <div className=" m-4 md:mx-16">
                        <Table
                            title="Reporte detallado de transito por vehiculo"
                            columns={columns}
                            data={transitData}
                        />
                    </div>
                </div>
            </Dialog>
        </>
    )
}

export default ReportDetail
