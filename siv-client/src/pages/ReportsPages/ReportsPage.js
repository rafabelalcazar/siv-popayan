import React, { useState } from 'react'
import { Button, Typography, Icon } from '@material-ui/core'
import { Details, Info } from '@material-ui/icons';
import Table from '../../components/Table'
import { DateTimePicker, KeyboardDatePicker } from '@material-ui/pickers';
import axios from 'axios';
import { getDistanceFromLatLonInKm } from '../../utils/getDistanceCoords';
import { getLatLngFromPayload } from '../../utils/utils';
import moment from 'moment';
import ReportDetail from './components/ReportDetail';

/**
* Render UI for all vehicles with in a time interval
*
*/
const ReportsPage = () => {

    const [initialDate, setInitialDate] = useState()
    const [endDate, setEndDate] = useState()
    const [vehiclesReport, setVehiclesReport] = useState()
    const [hardwareSerial, setHardwareSerial] = useState()

    const [open, setOpen] = useState(false)

    const handleClose = () => {
        setOpen(false);
    }

    console.log(vehiclesReport)

    const columns = [
        {
            title: 'Serial',
            field: 'hardware_serial'
        },
        // {
        //     title: 'placa',
        //     field: 'placa'
        // },
        {
            title: 'Velocidad promedio',
            field: 'velocity_avg',
            render: rowData => <Typography>{rowData.velocity_avg.toFixed(2)} Km/hr</Typography>
        },
        {
            title: 'Distancia total',
            field: 'total_distance',
            render: rowData => <Typography>{rowData.total_distance.toFixed(2)} Km</Typography>
        },
        {
            title: 'Ultima ubicacion reportada',
            field: 'last_point'
        },
        {
            title: 'Hora último reportee',
            field: 'last_point_time_moment',
            render: rowData => <Typography>{rowData.last_point_time_moment.toString()}</Typography>
        },

    ]

    const handleGenerateReport = () => {
        const getTransits = async () => {
            const url = `${process.env.REACT_APP_ENDPOINT}/api/reports/`;
            const { data } = await axios.post(url, {
                initialDate,
                endDate
            })
            return data
        }

        getTransits().then((res) => {
            const { transits } = res
            console.log(transits)
            const dataVehicles = transits.reduce((acc, current) => {
                // console.log(acc[current?.hardware_serial])
                if (!acc.hasOwnProperty(current?.hardware_serial)) {
                    return ({
                        ...acc,
                        [current?.hardware_serial]: [current]
                    })
                }
                else {
                    return ({
                        ...acc,
                        [current?.hardware_serial]: [
                            ...acc[current?.hardware_serial],
                            current,
                        ]
                    })

                }
            }, {})
            // console.log(dataVehicles)

            const vehiclesSerials = Object.keys(dataVehicles)

            const vehiclesDataFormat = vehiclesSerials.map(vehicle => {
                const lengthData = dataVehicles[vehicle].length
                const velocity_avg = dataVehicles[vehicle].reduce((acc, current) => (acc | 0) + Number(current.velocity), 0) / lengthData
                const total_distance = dataVehicles[vehicle].reduce((acc, current, idx) => {
                    const { lat, lng } = getLatLngFromPayload(current.payload_fields)
                    const { lat: next_lat, lng: next_lng } = getLatLngFromPayload(dataVehicles[vehicle][idx + 1]?.payload_fields ? dataVehicles[vehicle][idx + 1]?.payload_fields : current.payload_fields)
                    return acc + Number(getDistanceFromLatLonInKm([lat, lng], [next_lat, next_lng]) || 0)
                }, 0)
                const last_point = dataVehicles[vehicle][0].payload_fields
                const last_point_time = dataVehicles[vehicle][0].creation_date
                return ({
                    hardware_serial: vehicle,
                    velocity_avg,
                    last_point,
                    total_distance,
                    last_point_time: last_point_time,
                    last_point_time_moment: moment(last_point_time)
                })
            })

            setVehiclesReport(vehiclesDataFormat)
        })
    }


    return (
        <div>

            <div className="my-4">
                <div>
                    <Typography>Seleccione un rango de fechas para generar reporte</Typography>
                    <div className="flex justify-between">
                        <DateTimePicker
                            className="w-1/3"
                            autoOk={true}
                            margin="normal"
                            id="date-picker-dialog"
                            label="Desde"
                            inputVariant="outlined"
                            variant="inline"
                            format="dd/MM/yyyy hh:mm a"
                            value={initialDate}
                            onChange={(e) => setInitialDate(e)}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                        <DateTimePicker
                            className="w-1/3"
                            autoOk={true}
                            margin="normal"
                            id="date-picker-dialog"
                            label="Hasta"
                            inputVariant="outlined"
                            variant="inline"
                            format="dd/MM/yyyy hh:mm a"
                            value={endDate}
                            onChange={(e) => setEndDate(e)}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                        <Button variant="contained" onClick={handleGenerateReport}>Generar reporte</Button>
                    </div>
                </div>
                <Table
                    title="Reporte general de vehiculos"
                    columns={columns}
                    data={vehiclesReport}
                    actions={[
                        {
                            icon: () => <Info className="text-blue-500 border rounded  mx-2" />,
                            tooltip: 'Ver detalle',
                            onClick: (e, rowData) => {
                                // console.log(e)
                                // console.log(rowData)
                                setHardwareSerial(rowData.hardware_serial)
                                setOpen(true)

                            }
                        }
                    ]}
                />
                <ReportDetail
                    open={open}
                    onClose={handleClose}
                    hardware_serial={hardwareSerial}
                    initialDate={initialDate}
                    endDate={endDate}
                />
            </div>
        </div>
    )
}

export default ReportsPage
