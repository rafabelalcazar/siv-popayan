import React from 'react'
import { t_btn } from '../../constants/tailwindStyles'
const NoRoutes = () => {
    return (
        <div className="pt-20 text-gray-800 px-4 md:px-32">
            <p className="my-4">Aun no hay Vehiculos registrados</p>
            <button className={t_btn} >Registrar Vehiculo</button>
        </div>
    )
}

export default NoRoutes
