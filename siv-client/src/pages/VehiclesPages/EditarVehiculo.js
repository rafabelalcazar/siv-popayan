import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import { TextField } from '@material-ui/core';
// import Alert from '../../components/Alert';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import { t_btn } from '../../constants/tailwindStyles';
import axios from 'axios';
import BackNavigation from '../../components/BackNavigation';

/**
* Render UI for edit a vehicle
*
*/
const EditarVehiculo = () => {
  const history = useHistory()
  // let { id } = useParams();

  const vehicle = useSelector(({ main }) => main.selectedVehicle)

  const dispatch = useDispatch()
  
  const [routeList, setRouteList] = useState([])

  /**
  * Request to get existing routes
  *
  */
   useEffect(() => {
    const url = `${process.env.REACT_APP_ENDPOINT}/api/routes/`;
    fetch(url)
      .then((resp) => resp.json())
      .then(routes => {
        setRouteList(routes.routes)
      })
    return () => {
      // console.log('Bye List routes')
    }
  }, [])

  const handleVehicleSelect = (e) => {
    // dispatch to Redux store
    dispatch(Actions.setSelectedVehicle({
      ...vehicle,
      [e.target.name]: e.target.value
    }))
  }
  const editeVehicleRequest = (e) => {
    console.log('Ready to edit vehicle', vehicle)
    axios.put(`${process.env.REACT_APP_ENDPOINT}/api/vehicles/${vehicle._id}`, {
      hardware_serial: vehicle.hardware_serial,
      number: vehicle.number,
      placa: vehicle.placa,
    }).then(res => {
        history.push('/vehicles')
        // console.log(res)
    })
        .catch(err => console.log(err))

  }

  return (
    <div>
      <div>
        <BackNavigation to='/vehicles' text='Editar vehículo' />
        <form className='h-screen py-16 md:py-24'>
          <div className="-mx-3 md:flex mb-6">
            <div className="md:w-1/2 px-3 mb-6 md:mb-0">
              <TextField
                className="w-full"
                label="Hardware serial"
                name="hardware_serial"
                variant="outlined"
                onChange={handleVehicleSelect}
                value={vehicle.hardware_serial}
              />
            </div>
            <div className="md:w-1/2 px-3 mb-6 md:mb-0">
              <TextField
                className="w-full"
                name="route"
                select
                value={vehicle.route}
                label="Ruta"
                variant="outlined"
                defaultValue=''
                SelectProps={{
                  native: true,
                }}
                onChange={handleVehicleSelect}
                helperText=""
              >
                <option></option>
                {
                  routeList.map((route) => {
                    console.log(route)
                    return <option key={route._id} value={route._id} > {route.name} </option>
                  })
                }

              </TextField>
            </div>
            <div className="md:w-1/2 px-3">
              <TextField
                className="w-full"
                label="Placa"
                name="placa"
                variant="outlined"
                onChange={handleVehicleSelect}
                value={vehicle.placa}
              />
            </div>
          </div>
          <div className="md:w-full flex justify-center my-4">
          <button type="button" onClick={editeVehicleRequest} className={`${t_btn} w-full`} >
              Editar Vehículo
                </button>
          </div>
        </form>
      </div>
    </div>

  )
}

export default EditarVehiculo

