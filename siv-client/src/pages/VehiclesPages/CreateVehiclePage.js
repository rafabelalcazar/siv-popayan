import React, { useEffect, useState } from 'react'
import { Snackbar, TextField } from '@material-ui/core';
import Alert from '../../components/Alert';
import { t_btn } from '../../constants/tailwindStyles';
import axios from 'axios';
import BackNavigation from '../../components/BackNavigation';

/**
* Render UI for create a vehicle
*
*/
const CreateVehiclePage = () => {
  const [openSuccess, setOpenSuccess] = useState(false)
  const [openError, setOpenError] = useState(false)
  const [vehicle, setVehicle] = useState({
    hardware_serial: '',
    route: '',
    placa: ''
  })

  const [routeList, setRouteList] = useState([])

  const url = `${process.env.REACT_APP_ENDPOINT}/api/routes/`;

  /**
  * Request to get existing routes 
  *
  */
  useEffect(() => {

    fetch(url)
      .then((resp) => resp.json())
      .then(routes => {
        setRouteList(routes.routes)
      })
    return () => {
      // console.log('Bye List routes')
    }
  }, [])

  const handleChange = (e) => {
    setVehicle(vehicle => {
      if (e.target.name === 'placa') {
        return ({
          ...vehicle,
          [e.target.name]: e.target.value.toUpperCase()
        })
      }
      return ({
        ...vehicle,
        [e.target.name]: e.target.value
      })
    })
  }
  const createVehicleRequest = () => {
    
    axios.post(`${process.env.REACT_APP_ENDPOINT}/api/vehicles`, {
      hardware_serial: vehicle.hardware_serial,
      route: vehicle.route,
      placa: vehicle.placa,
    }).then(resp => {
      setOpenSuccess(true)
      // history.push('/routes')

    })
      .catch(err => {
        setOpenError(true)
      })

  }

  return (
    <div>
      <div className=' '>
        <BackNavigation to='/vehicles' text='Crear vehículo' />
        <form className='h-screen py-16 md:py-24'>
          <div className="-mx-3 md:flex mb-6">
            <div className="md:w-1/2 px-3 mb-6 md:mb-0">
              <TextField
                className="w-full"
                label="Hardware serial"
                name="hardware_serial"
                variant="outlined"
                onChange={handleChange}
                value={vehicle.hardware_serial}
              />
            </div>
            <div className="md:w-1/2 px-3 mb-6 md:mb-0">
              <TextField
                className="w-full"
                name="route"
                select
                value={vehicle.route}
                label="Ruta"
                variant="outlined"
                defaultValue=''
                SelectProps={{
                  native: true,
                }}
                onChange={handleChange}
                helperText=""
              >
                <option></option>
                {
                  routeList.map((route) => {
                    return <option key={route._id} value={route._id} > {route.name} </option>
                  })
                }

              </TextField>
            </div>
            <div className="md:w-1/2 px-3">
              <TextField
                className="w-full"
                label="Placa"
                name="placa"
                variant="outlined"
                onChange={handleChange}
                value={vehicle.placa}
              />
            </div>
          </div>
          <div className="md:w-full flex justify-center my-4">
            <button type="button" onClick={createVehicleRequest} className={`${t_btn} w-full`}>
              Registrar Vehículo
            </button>
          </div>
        </form>

      </div>
    </div>
  )
}


export default CreateVehiclePage

