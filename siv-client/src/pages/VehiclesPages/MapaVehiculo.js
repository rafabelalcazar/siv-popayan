import './vehicles.css'
import React, { useEffect, useContext, useState } from 'react'
import { connect } from 'react-redux'
import {  useHistory } from 'react-router-dom';
// import { t_btn } from '../../constants/tailwindStyles';
// import * as Actions from '../../store/actions';

import mapboxgl from 'mapbox-gl'
import { ArrowBack } from '@material-ui/icons';
import { SocketContext } from '../../context/SocketContext';
import useMapbox from '../../hooks/useMapbox';
mapboxgl.accessToken = 'pk.eyJ1IjoicmFmYWJlbGFsY2F6YXIiLCJhIjoiY2tteGo0cmk3MDZxZjJ2bXUwMHRyOWhmOSJ9.54HQH5wF2poXqTlRcTpRAA';


const initialPoint = {
    lng: -76.613,
    lat: 2.44923,
    zoom: 13
}

/**
* Render UI to show vehicles on map
*
*/
const MapaVehiculo = ({ main }) => {
    // const { selectedRoute: route, user } = main

    const { socket } = useContext(SocketContext)
    const { setRef, map } = useMapbox(initialPoint)
    const [markers, setMarkers] = useState({})

    // const dispatch = useDispatch()
    const history = useHistory()

    // setMakers
    const updateMarkers = (dataVehicle) => {
        const jsondata = dataVehicle.payload_fields.substring(1, dataVehicle.payload_fields.length - 1)
        const [lat, lng] = jsondata.replace(/'latitude_gps':/gi, ``).replace(/'longitude_gps':/gi, '').split(',')
        const newMarkers = markers
        newMarkers[dataVehicle.hardware_serial] = { lng: Number(lng), lat: Number(lat), route: dataVehicle.route }
        setMarkers(currentMarkers => ({ ...currentMarkers, ...newMarkers }))
    }

    // Listen all vehicles by hardware_serial
    useEffect(() => {
        socket.on('00BA51454A3204B6', (data) => {
            updateMarkers(data)
        })
        socket.on('00712A6BFD5FD6D2', (data) => {
            updateMarkers(data)
        })
        socket.on('0056A03AAAE4F07C', (data) => {
            updateMarkers(data)
        })
        socket.on('00EB0D71D1A0FAD7', (data) => {
            updateMarkers(data)
        })
        socket.on('00923D037D73DE54', (data) => {
            updateMarkers(data)
        })
    }, [])

    // Update markers position
    useEffect(() => {
        const h_serials = Object.keys(markers)
        const arrayMarkers = []
        h_serials.forEach(serial => {
            let el = document.createElement('div')
            el.innerHTML = `<h1>${markers[serial]?.route?.name}</h1>`
            el.className = 'vehicleMarker'
            el.style.backgroundColor = `${markers[serial]?.route?.color}`
            const mark = new mapboxgl.Marker(el).setLngLat([markers[serial].lng, markers[serial].lat])
                .setPopup(new mapboxgl.Popup({ offset: 25 })
                    .setHTML('<h3>' + serial + '</h3><p>'))
                .addTo(map.current)
            arrayMarkers.push(mark)
        })
        return () => arrayMarkers.map(m => m.remove())
    }, [markers])
    

    const regresar = () => {
        history.push('/vehicles')
    }
    return (
        <div>
            <div className='absolute top-0 right-0 left-0 bottom-0 ' >
                <div ref={setRef} className="w-full flex flex-1 h-full rounded-lg" />
            </div>
            <div className='fixed z-10 left-2 bg-white bg-opacity-90 rounded p-1 md:p-4' onClick={regresar}>
                <ArrowBack />
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        main: state.main
    }
}

export default connect(mapStateToProps, null)(MapaVehiculo)

