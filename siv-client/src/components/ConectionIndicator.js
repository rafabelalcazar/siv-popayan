import React from 'react'

const ConectionIndicator = ({ online }) => {
    return (
        <div className="flex items-center">
            <p className={`text-${online ? 'green' : 'red'}-500 pr-2`} >{online ? 'Online' : 'Offline'}</p>
            <div className={`bg-${online ? 'green' : 'red'}-500 relative rounded-full h-3 w-3`}>
                <span className={`animate-ping absolute inline-flex rounded-full h-3 w-3 bg-${online ? 'green' : 'red'}-500`}></span>
            </div>
        </div>
    )
}

export default ConectionIndicator
