import React from 'react'
import { AccountCircle, Directions, DirectionsBus, Feedback, Flag, Explore, StopOutlined, Stop, HomeWork } from "@material-ui/icons";
import { Link } from 'react-router-dom';

const Drawer = ({ open, setOpen, user }) => {
    return (
        <aside
            className={`transform bg-blue-500 top-0 left-0 w-64 bg-white fixed h-full overflow-auto ease-in-out transition-all duration-300 z-30 shadow-cl ${open ? 'translate-x-0' : '-translate-x-full'}`}
        >
            <h2 className="text-center text-white font-semibold p-5" >Bienvenido usuario</h2>
            <nav>
                <ul>
                    <li>
                        <Link to="/routes" onClick={() => setOpen(false)} className="px-5 text-white hover:bg-blue-400 p-4 cursor-pointer block" >
                            <Directions />
                            <span className="ml-4">Rutas</span>
                        </Link>
                    </li>
                    {
                        user?.role === 'TRAVELER' ? null :
                            <li>
                                <Link to="/vehicles" onClick={() => setOpen(false)} className="px-5 text-white hover:bg-blue-400 p-4 cursor-pointer block">
                                    <DirectionsBus />
                                    <span className="ml-4">Vehiculos</span>
                                </Link>
                            </li>
                    }
                    {
                        user?.role === 'TRAVELER' ? null :
                            <li>
                                <Link to="/reports" onClick={() => setOpen(false)} className="px-5 text-white hover:bg-blue-400 p-4 cursor-pointer block">
                                    <Flag />
                                    <span className="ml-4">Reportes</span>
                                </Link>
                            </li>
                    }
                    <li>
                        <Link to="/startTripFrom" onClick={() => setOpen(false)} className="px-5 text-white hover:bg-blue-400 p-4 cursor-pointer block">
                            <Explore />
                            <span className="ml-4">Iniciar viaje</span>
                        </Link>
                    </li>
                    <li>
                        <Link to="/event" onClick={() => setOpen(false)} className="px-5 text-white hover:bg-blue-400 p-4 cursor-pointer block">
                        {/* <Link to="/create-event" onClick={() => setOpen(false)} className="px-5 text-white hover:bg-blue-400 p-4 cursor-pointer block"> */}
                            <Feedback />
                            <span className="ml-4">Novedades</span>
                        </Link>
                    </li>
                    <li>
                        <Link to="/stations" onClick={() => setOpen(false)} className="px-5 text-white hover:bg-blue-400 p-4 cursor-pointer block">
                            <HomeWork/>
                            <span className="ml-4">Estaciones</span>
                        </Link>
                    </li>
                    <li>
                        <Link to="/account" onClick={() => setOpen(false)} className="px-5 text-white hover:bg-blue-400 p-4 cursor-pointer block" >
                            <AccountCircle />
                            <span className="ml-4">Mi Cuenta</span>
                        </Link>
                    </li>
                </ul>
            </nav>

        </aside>
    )
}

export default Drawer
