import React from 'react'
import { ArrowBack } from '@material-ui/icons';
import { useHistory } from 'react-router-dom';

const BackNavigation = ({to, text}) => {

    const history = useHistory()

    const regresar = () => {
        // history.push('/vehicles')
        history.push(to)
    }

    return (
        <div className='flex justify-between backdrop-blur fixed left-0 right-0 items-center p-3 w-full bg-white  shadow rounded z-10' >
            <div className='flex items-center justify-center cursor-pointer' >
                <ArrowBack onClick={regresar} />
            </div>
            <div className=' font-bold text-center'>{text}</div>
            <div className=''></div>
        </div>
    )
}

export default BackNavigation
