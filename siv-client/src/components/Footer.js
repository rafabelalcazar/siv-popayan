import React from 'react'

const Footer = () => {
    return (
        <div class="bg-gray-100 shadow-xl" >
            <div className='flex mx-auto container' >
                <div className='my-8'>
                    <p>Sistema de Información al Viajero</p>
                    <p>SIV Popayán</p>

                </div>
            </div>
        </div>
    )
}

export default Footer
