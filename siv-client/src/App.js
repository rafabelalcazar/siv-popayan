import { Menu } from "@material-ui/icons";
import { useContext, useState, useEffect } from "react";
import Drawer from "./components/Drawer";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import VehiclesPage from "./pages/VehiclesPages/VehiclesPage";
import ReportsPage from "./pages/ReportsPages/ReportsPage";
import RoutesListPage from "./pages/RoutesPages/RoutesListPage";
import CreateRoutePage from "./pages/RoutesPages/CreateRoutePage";
import RoutesPage from "./pages/RoutesPages/RoutesPage";
import RouteEdit from "./pages/RoutesPages/RouteEdit";
import VerMapa from "./pages/RoutesPages/VerMapa";
import MapEdit from "./pages/RoutesPages/MapEdit";
import CreateVehiclePage from "./pages/VehiclesPages/CreateVehiclePage";
import MapaVehiculo from "./pages/VehiclesPages/MapaVehiculo";
import EditarVehiculo from "./pages/VehiclesPages/EditarVehiculo";
import { SocketContext } from "./context/SocketContext";
import ConectionIndicator from "./components/ConectionIndicator";
import StartTrip from "./pages/StartTripPages/StartTrip";
import StartTripToPoint from "./pages/StartTripPages/StartTripToPoint";
import ResumeTrip from "./pages/StartTripPages/ResumeTrip";
import StationPage from "./pages/StationPages/StationPage";
import EventPage from "./pages/EventsPages/EventPage";
import EventMapPage from "./pages/EventsPages/EventMapPage";
import Register from "./pages/Auth/Register";
import MyAccount from "./pages/Auth/MyAccount";
import Login from "./pages/Auth/Login";
import { onAuthStateChanged } from "firebase/auth";
import { getFirestore, doc, getDoc } from 'firebase/firestore'
import { app, auth } from "./firebaseConfig";
import { connect, useDispatch } from 'react-redux';
import * as Actions from "./store/actions/main.actions";
import RoutesListTravelerPage from "./pages/RoutesPages/RoutesListTravelerPage";
import StationListPage from "./pages/StationPages/StationListPage";

const firestore = getFirestore(app)

/**
 * Root component for render WebApp.
 *
 */
function App({ main }) {
  const { user } = main
  const [open, setOpen] = useState(false)
  const [loading, setLoading] = useState(true)
  const { online } = useContext(SocketContext)

  const dispatch = useDispatch()

  /**
  * Get user data and store it.
  *
  */
  const getUserData = async (user) => {
    if (!user?.uid) {
      dispatch(Actions.setUserData(user))
      return

    }

    const document = doc(firestore, 'users', user?.uid)
    const docSnap = await getDoc(document);
    if (docSnap.exists()) {
      const userData = {
        ...docSnap.data(),
        uid: user?.uid
      }
      dispatch(Actions.setUserData(userData))
      setLoading(false)
    } else {
      // doc.data() will be undefined in this case
      console.log("No such document!");
      setLoading(false)
    }

  }

  /**
  * Listen changes to the user authentication
  *
  */
  useEffect(() => {
    const subscription = onAuthStateChanged(auth, user => {
      if (user) {
        getUserData(user)
      } else {
        getUserData({})
        setLoading(false)
      }
    })

    return () => {
      subscription()
    }
  }, [])

  /**
  * Render the main view, every Route component has its path, more routes can be added using this component <Route><Route/> 
  *
  */
  return (
    <div className="" >
      <nav
        className="flex fixed w-full items-center justify-between px-6 h-16 bg-white text-gray-700 border-b border-gray-200 z-50 shadow-md"
      >
        {
          user?.uid ? <Menu className="cursor-pointer" onClick={() => setOpen(!open)} /> : null
        }

        <ConectionIndicator online={online} />
        <h1>SIV Popayán</h1>
      </nav>
      {
        loading ? null : <Router>
          <Drawer open={open} setOpen={setOpen} user={user} />
          <main className="h-screen w-full " onClick={() => setOpen(false)} >
            <div className="pt-16 text-gray-800 px-4 md:px-32">
              <Switch >
                {
                  !!user?.uid ?
                    <>
                      {/* SECTION RUTAS */}
                      <Route exact path="/routes">
                        {
                          user?.role == 'TRAVELER' ? <RoutesListTravelerPage /> :
                            <RoutesListPage />
                        }
                      </Route>
                      <Route exact path="/createRoute" >
                        <CreateRoutePage />
                      </Route>
                      <Route exact path="/routePath">
                        <RoutesPage />
                      </Route>
                      <Route exact path="/routeEdit/:id" >
                        <RouteEdit />
                      </Route>
                      <Route exact path="/verMapa" >
                        <VerMapa />
                      </Route>
                      <Route exact path="/mapEdit/:id" >
                        <MapEdit />
                      </Route>
                      
                      {/* SECTION VEHICULOS */}
                      <Route exact path="/vehicles" >
                        <VehiclesPage />
                      </Route>
                      <Route exact path="/createVehicle" >
                        <CreateVehiclePage />
                      </Route>
                      <Route exact path="/mapaVehiculo" >
                        <MapaVehiculo />
                      </Route>
                      <Route exact path="/editarVehiculo/:id" >
                        <EditarVehiculo />
                      </Route>

                      {/* SECTION REPORTES */}
                      <Route exact path="/reports" >
                        <ReportsPage />
                      </Route>

                      {/* SECTION "INICIAR VIAJE" */}
                      <Route exact path="/startTripFrom" >
                        <StartTrip />
                      </Route>
                      <Route exact path="/startTripTo" >
                        <StartTripToPoint />
                      </Route>
                      <Route exact path="/resumeTrip" >
                        <ResumeTrip />
                      </Route>

                      {/* SECTION "EVENTOS(NOVEDADES)" */}
                      <Route exact path="/create-event" >
                        <EventPage />
                      </Route>
                      <Route exact path="/event" >
                        <EventMapPage />
                      </Route>

                      {/* SECTION "ESTACIONES" */}
                      <Route exact path="/stations" >
                        <StationListPage />
                      </Route>
                      <Route exact path="/stations/:id" >
                        <StationPage />
                      </Route>
                      
                      {/* SECTION "MI CUENTA" */}
                      <Route exact path='/account' >
                        <MyAccount />
                      </Route>
                      <Redirect to="/routes" />
                    </> :
                    <>
                      <Route exact path='/login' >
                        <Login />
                      </Route>
                      <Route exact path='/register' >
                        <Register />
                      </Route>
                      <Redirect to="/login" />
                    </>
                }

              </Switch>
            </div>
          </main>
        </Router>
      }

    </div >
  );
}

const mapStateToProps = state => {
  return {
    main: state.main
  }
}

export default connect(mapStateToProps, null)(App);
