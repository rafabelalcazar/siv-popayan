import axios from 'axios';

export const SET_USER = 'USER: SET_USER_DATA'
export const SET_ROUTE = 'USER ENTERPRISE: SET ROUTE DATA';
export const SET_DATAROUTE = "Empresa de Transporte: Guardar Número y Tarifa"
export const SET_SELECTED_ROUTE = "Empresa de Transporte: SET SELECTED ROUTE"
export const SET_SELECTED_VEHICLE = "Empresa de Transporte: SET SELECTED VEHICLE"
export const GET_CURRENT_ROUTE = "Empresa de Transporte: GET CURRENT ROUTE"
export const UPDATE_CURRENT_ROUTE_PATH = "Empresa de Transporte: UPDATE CURRENT ROUTE PATH"
export const ADD_MARKER_TO_CURRENT_ROUTE_PATH = "Empresa de Transporte: ADD MARKER TO CURRENT ROUTE PATH"


export const setUserData = (user) => ({
    type: SET_USER,
    payload: user
})

export const setRoutePath = (route) => ({
    type: SET_ROUTE,
    payload: route
})

export const setDataRoute = (route) => ({
    type: SET_DATAROUTE,
    payload: route
})


export const setSelectedRoute = (route) => ({
    type: SET_SELECTED_ROUTE,
    payload: route
})

export const setSelectedVehicle = (vehicle) => ({
    type: SET_SELECTED_VEHICLE,
    payload: vehicle
})

export const getRoute = (id) => async (dispatch) => {

    const url = `${process.env.REACT_APP_ENDPOINT}/api/routes/${id}`;
    const data = await axios.get(url)

    console.log(data.data.route)
    dispatch({
        type: SET_SELECTED_ROUTE,
        payload: data.data.route
    })
}

export const setPathToEdit = (pathUpdate) => (dispatch) => {
    const { id, lng, lat } = pathUpdate
    dispatch({
        type: UPDATE_CURRENT_ROUTE_PATH,
        payload: { id, lng, lat }
    })
}

export const addMarkerToPath = (marker) => (dispatch) => {
    dispatch({
        type: ADD_MARKER_TO_CURRENT_ROUTE_PATH,
        payload: marker
    })
}