

export const SET_START_TRIP_FROM = 'USER TRAVELER: SET START TRIP FROM'
export const SET_START_TRIP_TO = 'USER TRAVELER: SET START TRIP TO'

export const setStartTripFrom = (point) => ({
    type: SET_START_TRIP_FROM,
    payload: point
})
export const setStartTripTo = (point) => ({
    type: SET_START_TRIP_TO,
    payload: point
})