import * as Actions from "../actions"
import produce from "immer"

const initialState = {
    user: {},
    route: {
        name: "",
        fare: '',
        path: []
    },
    selectedRoute: {
        path: []
    },
    selectedVehicle: {}
}

const mainReducer = produce((draft, action) => {
    switch (action.type) {
        case Actions.SET_USER:
            draft.user = action.payload
            return draft
        case Actions.SET_ROUTE:
            draft.route.path = action.payload
            return draft
        case Actions.SET_DATAROUTE:
            draft.route = { ...draft.route, ...action.payload }
            return draft
        case Actions.SET_SELECTED_ROUTE:
            draft.selectedRoute = { ...draft.selectedRoute, ...action.payload }
            return draft
        case Actions.UPDATE_CURRENT_ROUTE_PATH:
            const { id, lng, lat } = action.payload
            const idx = draft.selectedRoute.path.findIndex(item => item.id === id)
            if (idx !== -1) {
                draft.selectedRoute.path[idx] = { ...draft.selectedRoute.path[idx], lng, lat }
            }
            return draft
        case Actions.ADD_MARKER_TO_CURRENT_ROUTE_PATH:
            console.log(action.payload)
            draft.selectedRoute.path = [...draft.selectedRoute.path, action.payload]
            return draft
        case Actions.SET_SELECTED_VEHICLE:
            draft.selectedVehicle = action.payload
            return draft






        default:
            break;
    }
}, initialState)

export default mainReducer