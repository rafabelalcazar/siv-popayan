import { combineReducers } from "redux";
import main from "./main.reducer"
import trip from "./trip.reducer"


const reducer = combineReducers({
    main,
    trip

})

export default reducer