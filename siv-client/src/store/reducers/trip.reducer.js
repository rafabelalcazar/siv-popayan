import produce from 'immer'

import * as Actions from '../actions'

const initialState = {
    trip: {
        from: {},
        to: {}
    }
}

const tripReducer = produce((draft, action) => {
    switch (action.type) {
        case Actions.SET_START_TRIP_FROM:
            draft.trip.from = action.payload
            return draft
        case Actions.SET_START_TRIP_TO:
            draft.trip.to = action.payload
            return draft
        default:
            break;
    }
}, initialState)

export default tripReducer