import { createContext } from "react"
import useSocket from "../hooks/useSocket"

export const SocketContext = createContext()

/**
* Allow to access to the socket from any children component
*/
export const SocketProvider = ({ children }) => {

    const { socket, online, setParams } = useSocket(process.env.REACT_APP_ENDPOINT)

    return <SocketContext.Provider value={{ socket, online, setParams }} >
        {children}
    </SocketContext.Provider>
}
