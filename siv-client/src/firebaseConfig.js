// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDXvFBJBhA3yIzdCMCl3VlayUaKNz3Vmto",
  authDomain: "sivpopayan.firebaseapp.com",
  projectId: "sivpopayan",
  storageBucket: "sivpopayan.appspot.com",
  messagingSenderId: "1027880417413",
  appId: "1:1027880417413:web:b983fda85346fb0320d1ec",
  measurementId: "G-3NR18TYFKQ"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app)

const analytics = getAnalytics(app);