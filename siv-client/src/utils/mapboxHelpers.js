import mapboxgl from 'mapbox-gl'
mapboxgl.accessToken = 'pk.eyJ1IjoicmFmYWJlbGFsY2F6YXIiLCJhIjoiY2tteGo0cmk3MDZxZjJ2bXUwMHRyOWhmOSJ9.54HQH5wF2poXqTlRcTpRAA';

/**
 * @param {Object}  options object to render marker.
 * @param {Array}   position Array with [lng,lat].
 * @return {Object}  Return a mapbox marker.
 */
export const createMarker = ({ options, position, map }) => {
    return new mapboxgl.Marker(options).setLngLat(position).addTo(map)
}