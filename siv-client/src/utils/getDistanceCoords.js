/**
 * Returns distance between to points.
 *
 * @param {[number, number]} P1 [lat, lng] coords Point 1.
 * @param {[number,number]} P2 [lat, lng] coords Point 2.
 * @return {number} distance between P1 and P2.
 */
export function getDistanceFromLatLonInKm(P1, P2) {
    const lat1 = P1[0]
    const lon1 = P1[1]

    const lat2 = P2[0]
    const lon2 = P2[1]

    const R = 6371; // Radius of the earth in km
    const dLat = deg2rad(lat2 - lat1);  // deg2rad below
    const dLon = deg2rad(lon2 - lon1);
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c; // Distance in km
    return d;
}

// Converts from degrees to radians.
function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

// Converts from radians to degrees.
function rad2deg(radians) {
    return radians * 180 / Math.PI;
}

/**
 * Returns all routes within a specific radius from a reference point.
 *
 * @param {[routes]} routes routes with path key.
 * @param {[number,number]} point [lat, lng] reference point coords.
 * @param {number} maxDistance (Km) maximun distance - default 1.
 * @return {[routes]} array of routes that are within the radius.
 */
export function getNearRoutes(routes, point, maxDistance = 1) {
    const filteredRoutes = routes.filter(r => {
        const pointNear = r.path.filter((p, idx) => getDistanceFromLatLonInKm([p.lat, p.lng], point) < maxDistance)
        return pointNear.length > 0
    })
    return filteredRoutes
}

export function getNearRoutesTrajectory(routes, fromPoint, ToPoint) {
    const filteredRoutes = routes.map(r => {
        return ({
            ...r,
            nearPointFrom: r.path.findIndex(p => getDistanceFromLatLonInKm([p.lat, p.lng], fromPoint) < 1),
            nearPointTo: r.path.findIndex(p => getDistanceFromLatLonInKm([p.lat, p.lng], ToPoint) < 1)
        })
    }).filter(r => r.nearPointFrom <= r.nearPointTo)
    return filteredRoutes
}

export function calculateBearing(startLat, startLng, destLat, destLng) {
    startLat = deg2rad(startLat);
    startLng = deg2rad(startLng);
    destLat = deg2rad(destLat);
    destLng = deg2rad(destLng);

    const y = Math.sin(destLng - startLng) * Math.cos(destLat);
    const x = Math.cos(startLat) * Math.sin(destLat) -
        Math.sin(startLat) * Math.cos(destLat) * Math.cos(destLng - startLng);
    const brng = rad2deg(Math.atan2(y, x));
    return (brng + 360) % 360;
}

export function getNearPointInTrajectory(route, userPoint) {
    const distanceToUser = route.path.map(p => getDistanceFromLatLonInKm([p.lat, p.lng], userPoint))
    const minValue = Math.min(...distanceToUser)
    const idx = distanceToUser.findIndex(value => value === minValue)
    console.log('INDEX', idx)
    return idx
}

export const getCenterFromPath = (path) => {
    const lats = path.map(c => c.lat)
    const lngs = path.map(c => c.lng)

    const maxLat = Math.max(...lats)
    const minLat = Math.min(...lats)

    const maxLng = Math.max(...lngs)
    const minLng = Math.min(...lngs)

    const center = {
        lat: (maxLat + minLat) / 2,
        lng: (maxLng + minLng) / 2
    }
    return ({ center, maxLat, minLat, maxLng, minLng })

}

export const getZoomValue = ({ maxLat, minLat, maxLng, minLng }) => {

    const latDistance = (maxLat - minLat)
    const lngDistance = (maxLng - minLng)

    const maxDistance = Math.max(latDistance, lngDistance)
    const zoomValue = -35*maxDistance + 15
    return zoomValue

}