export function getLatLngFromPayload(payload_fields) {
    const jsondata = payload_fields.substring(1, payload_fields.length - 1)
    const [lat, lng] = jsondata.replace(/'latitude_gps':/gi, ``).replace(/'longitude_gps':/gi, '').split(',')

    return {lat : Number(lat), lng : Number(lng)}
}