export const t_btn = 'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded outline:none'
export const t_btn_action = 'bg-yellow-600 hover:bg-yellow-700 text-white font-bold py-2 px-4 rounded outline:none'
export const t_btn_danger = 'bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 rounded outline:none'