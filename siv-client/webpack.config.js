const path = require('path')

module.exports = {
  entry: path.resolve(__dirname, 'src', 'index.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  use: {
    loader: 'babel-loader',
    options: {
      ignore: [ './node_modules/mapbox-gl/dist/mapbox-gl.js' ]
    }
  }
}